#ifndef TREEMODEL_HPP
#define TREEMODEL_HPP

#include <QAbstractItemModel>
#include <QPixmap>
#include <QItemSelection>
#include <QFileDevice>
#include <memory>
#include <map>
#include <set>
#include "nitrofs.hpp"
#include "metadata.hpp"

struct NitroDirectory;

//A model for the Nitro file system, represented by a tree.
class TreeModel : public QAbstractItemModel
{
public:
    explicit TreeModel(std::unique_ptr<NitroDirectory> root_directory, std::unique_ptr<NitroDirectoryMetadata> root_metadata);

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;

    QString Tree_View_Selection_Changed(const QModelIndexList& selected_indexes);

    /*!
     * \brief Answers the question: with this selection, do I have only a single file selected, and nothing else?
     */
    bool Single_File_is_Selected(const QModelIndexList& selected_indexes);

    /*!
     * \brief Obtains the file information from a tree model index.
     * \return The file information if index corresponds to a file, and not a directory.
     */
    NitroFile * Get_Selected_File(const QModelIndex& index);

    /*!
      * \brief Extract all the files and directories pointed to by indexes.
     * \note The base path NEEDS to be writable; this will not be checked
     */
    std::set<NitroFile *> Extract_Indexes(QString const& base_path, QFileDevice &nds_file, const QModelIndexList& indexes);

    NitroNodeMetadata* Find_Equivalent_Metadata_Node(NitroNode const* nitrofs_node) const;

    void Change_Compression_Type_to_Raw(NitroFile const* nitrofile);

    void Start_File_Type_Replacing(QModelIndex const&);

    void End_File_Type_Replacing();

private:
    const std::unique_ptr<NitroDirectory> root_directory_nitro {};
    std::unique_ptr<NitroDirectoryMetadata> root_directory_metadata {};
    const QPixmap icon_directory{}, icon_file {}, icon_NARC {};

    QString Get_String_For_Row(QModelIndex const& index) const;
    QVariant Get_Icon_For_Row(QModelIndex const& index) const;
    QString Get_Tool_Tip_For_Row(QModelIndex const& index) const;
    QString Get_Info_String_for_File(const NitroFile* const file_pointer) const noexcept;
    QString Get_Info_String_for_Directory(const NitroDirectory* const directory_pointer) const noexcept;
    QString Get_Info_String_for_Several_Selections(const QModelIndexList& selected_indexes);

    /*!
     * \brief Extract a file from the NitroROM file system
     * \param The path to a directory on the computer where the file should be saved. This directory needs to be writable.
     * \param The NDS ROM file from which the file should be extracted
     * \param The NitroROM file information
     * \return Whether the file was successfully extracted
     */
    bool Extract_File(QString const& base_path, QFileDevice& nds_file, const NitroFile* const file_pointer) const noexcept;
    std::set<NitroFile *> Extract_Directory(QString const& base_path, QFileDevice& nds_file, const NitroDirectory* const directory_pointer) const noexcept;

};

#endif // TREEMODEL_HPP
