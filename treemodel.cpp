#include "treemodel.hpp"
#include "nitrofs.hpp"
#include <QDebug>
#include <algorithm>
#include <QDir>
#include <QLocale>
#include <vector>
#include "algos.hpp"

using namespace std;

const int size_icon = 20;

TreeModel::TreeModel(unique_ptr<NitroDirectory> root_directory, std::unique_ptr<NitroDirectoryMetadata> root_metadata)
    : root_directory_nitro{move(root_directory)}, root_directory_metadata{move(root_metadata)},
      icon_directory{QPixmap{":/Folder.png"}.scaled(QSize{size_icon,size_icon}, Qt::KeepAspectRatio, Qt::SmoothTransformation)},
      icon_file{QPixmap{":/File.png"}.scaled(QSize{size_icon,size_icon}, Qt::KeepAspectRatio, Qt::SmoothTransformation)},
      icon_NARC{QPixmap{":/NARC.png"}.scaled(QSize{size_icon,size_icon}, Qt::KeepAspectRatio, Qt::SmoothTransformation)}
{
}

Qt::ItemFlags TreeModel::flags(const QModelIndex & index) const
{
    if (not index.isValid())
        return static_cast<Qt::ItemFlags>(0);
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant TreeModel::headerData(int , Qt::Orientation , int) const
{
    return QVariant{""};
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.column() > 0)
        return QVariant{};

    if (role == Qt::DisplayRole)
        return QVariant{this->Get_String_For_Row(index)};
    else if (role == Qt::DecorationRole)
        return this->Get_Icon_For_Row(index);
    else if (role == Qt::ToolTipRole)
        return QVariant{this->Get_Tool_Tip_For_Row(index)};
    else return QVariant{};
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    void* const pointer { (parent.isValid())? parent.internalPointer() : dynamic_cast<NitroNode*>(this->root_directory_nitro.get()) };
    const NitroNode* const node_pointer { reinterpret_cast<NitroNode*>(pointer) };
    const NitroDirectory* const directory_pointer { dynamic_cast<const NitroDirectory*>(node_pointer) };
    if (directory_pointer != nullptr) {
        const auto nb_children = directory_pointer->ChildrenNumber();
        return nb_children;
    }
    else return 0;
}

int TreeModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QModelIndex TreeModel::index(int _row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(_row, column, parent))
        return QModelIndex{};

    const size_t row { static_cast<size_t>(_row) };
    if (column > 0)
        return QModelIndex{};

    const NitroDirectory* pointer_directory {};
    const NitroNode* const pointer_node { reinterpret_cast<const NitroNode*>(
                (parent.isValid()) ?
                parent.internalPointer() : dynamic_cast<NitroNode*>(this->root_directory_nitro.get())) };
    pointer_directory = dynamic_cast<const NitroDirectory*>(pointer_node);
    if (pointer_directory == nullptr)
    {
        //Not a directory? Should never happen.
        qDebug() << "Tried to get an index from a child of a file\n";
        return QModelIndex{};
    }
    if (row < pointer_directory->ChildrenNumber())
    {
        //We create an index to a child directory
        if (row < pointer_directory->children_directories.size())
        {
            NitroDirectory* child_dir = pointer_directory->children_directories[row].get();
            NitroNode* child_node = dynamic_cast<NitroNode*>(child_dir);
            return createIndex(row, column, child_node);
        }
        //Otherwise we create an index to a child file
        else
        {
            //The files are all listed together after all(!) the directories
            const auto nb_dirs = pointer_directory->children_directories.size();
            NitroFile* child_file = pointer_directory->children_files[row - nb_dirs].get();
            NitroNode* child_node = dynamic_cast<NitroNode*>(child_file);
            return createIndex(row, column, child_node);
        }
    }
    return QModelIndex{};
}

int Find_Position_of_Child(const NitroDirectory* const alleged_parent, const NitroDirectory* const alleged_child)
{
    if (alleged_parent != nullptr && alleged_child != nullptr)
    {
        const auto iter_child = find_if(alleged_parent->children_directories.begin(),
                                     alleged_parent->children_directories.end(),
                                     [=](unique_ptr<NitroDirectory> const& uptr_dir)
                                        {
            auto* raw_ptr = uptr_dir.get();
            return raw_ptr == alleged_child;
        });
        if (iter_child != alleged_parent->children_directories.end())
            return distance(alleged_parent->children_directories.begin(), iter_child);
    }
    qDebug() << "Could not find row of child directory";
    return -1;
}

int Find_Position_of_Child(const NitroDirectory* const alleged_parent, const NitroFile* const alleged_child)
{
    if (alleged_parent != nullptr && alleged_child != nullptr)
    {
        const auto iter_child = find_if(alleged_parent->children_files.begin(),
                                     alleged_parent->children_files.end(),
                                     [=](unique_ptr<NitroFile> const& uptr_file)
                                        {
            return uptr_file.get() == alleged_child;
        });
        if (iter_child != alleged_parent->children_files.end())
            return distance(alleged_parent->children_files.begin(), iter_child) + alleged_parent->children_directories.size();
    }
    qDebug() << "Could not find row of child file";
    return -1;
}

QModelIndex TreeModel::parent(const QModelIndex &child) const
{
    if (not child.isValid())
        return QModelIndex{};

    NitroNode* const pointer_node { reinterpret_cast<NitroNode*>(child.internalPointer()) };
    NitroDirectory* const parent_directory { pointer_node->parent };
    if (parent_directory != this->root_directory_nitro.get())
    {
        const int row { Find_Position_of_Child(parent_directory->parent, parent_directory) };
        if (row == -1)
            qDebug() << "Parent row is -1";
        return createIndex(row, 0, dynamic_cast<NitroNode*>(parent_directory));
    }
    return QModelIndex{};   //This is for the root node
}

QString TreeModel::Get_String_For_Row(const QModelIndex &index) const
{
    if (!index.isValid())
        return "";

    const NitroNode* const node { reinterpret_cast<const NitroNode*>(index.internalPointer()) };
    if (node == nullptr)
        return "Invalid index";

    return node->name;
}

QVariant TreeModel::Get_Icon_For_Row(const QModelIndex &index) const
{
    if (!index.isValid())
        return QVariant{};

    const NitroNode* const node { reinterpret_cast<const NitroNode*>(index.internalPointer()) };
    if (node == nullptr)
        return QVariant{};

    const NitroNARC* const perhaps_a_narc_archive { dynamic_cast<const NitroNARC*>(node) };
    if (perhaps_a_narc_archive != nullptr)
    {
        return QVariant{icon_NARC};
    }
    const NitroDirectory* const maybe_a_directory { dynamic_cast<const NitroDirectory*>(node) };
    if (maybe_a_directory != nullptr)
    {
        return QVariant{icon_directory};
    }
    const NitroFile* const hopefully_a_file { dynamic_cast<const NitroFile*>(node) };
    if (hopefully_a_file != nullptr)
    {
        return QVariant{icon_file};
    }
    else
    {

        //If we get here, things just got VERY wrong
        qDebug() << "Try to get an icon for a Nitro Node that is neither a file or a directory or a NARC archive";
        return QVariant{};
    }
}

QString TreeModel::Get_Tool_Tip_For_Row(const QModelIndex &index) const
{
    const NitroNode* const node_pointer { reinterpret_cast<const NitroNode*>(index.internalPointer()) };
    const NitroDirectory* const directory_pointer { dynamic_cast<const NitroDirectory*>(node_pointer) };
    if (directory_pointer != nullptr)
        return Get_Info_String_for_Directory(directory_pointer);
    else
    {
        const NitroFile* const file_pointer { dynamic_cast<const NitroFile*>(node_pointer) };
        if (file_pointer != nullptr)
            return Get_Info_String_for_File(file_pointer);
        else
            return tr("Invalid item type");
    }
}

QString TreeModel::Get_Info_String_for_File(const NitroFile * const file_pointer) const noexcept
{
    QLocale system_locale{};
    return QString{tr("File id: 0x%1 - Begin address: 0x%2 - End address: 0x%3 - Size: %4 bytes")}
    .arg(QString::number(file_pointer->id, 16).toUpper())
    .arg(QString::number(file_pointer->start_offset, 16).toUpper())
    .arg(QString::number(file_pointer->end_offset, 16).toUpper())
    .arg(system_locale.toString(file_pointer->end_offset - file_pointer->start_offset));
}

QString TreeModel::Get_Info_String_for_Directory(const NitroDirectory * const directory_pointer) const noexcept
{
    const auto nb_dirs_and_files = directory_pointer->Count_Directories_and_Files();
    return QString{tr("Directory id: 0x%1 - %2 subdirectories total - %3 files total")}
       .arg(QString::number(directory_pointer->id, 16).toUpper())
       .arg(nb_dirs_and_files.first)
    .arg(nb_dirs_and_files.second);
}

QString TreeModel::Get_Info_String_for_Several_Selections(const QModelIndexList &selected_indexes)
{
    auto is_directory = [](QModelIndex const& index) {
        NitroNode* node { reinterpret_cast<NitroNode*>(index.internalPointer()) };
        NitroDirectory* maybe_dir { dynamic_cast<NitroDirectory*>(node) };
        return maybe_dir != nullptr;
    };
    //Change this once we implement NARC files, otherwise they will be counted twice.
    auto is_file = [](QModelIndex const& index) {
        NitroNode* node { reinterpret_cast<NitroNode*>(index.internalPointer()) };
        NitroFile* maybe_file { dynamic_cast<NitroFile*>(node) };
        return maybe_file != nullptr;
    };
    Q_UNUSED(is_file);

    const auto nb_dirs = count_if(selected_indexes.begin(), selected_indexes.end(), is_directory);
    //const auto nb_files = count_if(selected_indexes.begin(), selected_indexes.end(), is_file);
    const auto nb_files = selected_indexes.size() - nb_dirs;
    QString result {};
    if (nb_dirs + nb_files!= selected_indexes.size())
    {
        qDebug() << "Very bad: selected indexes are " << selected_indexes.size()<< ", dirs are " << nb_dirs << ", files are " << nb_files;
    }
    if (nb_dirs > 0)
        result = QString{tr("%1 director%2%3")}.arg(nb_dirs).arg((nb_dirs != 1)? tr("ies") : tr("y"))
                .arg((nb_files > 0)? ", " : "");
    if (nb_files > 0)
        result += QString{"%1 file%2"}.arg(nb_files).arg((nb_files != 1)? tr("s") : "");
    return result;
}

bool TreeModel::Extract_File(const QString &base_path, QFileDevice& nds_file, const NitroFile * const file_pointer) const noexcept
{
    const QDir directory_info { base_path };

    const auto* metadata_node = this->Find_Equivalent_Metadata_Node(file_pointer);
    const auto* metadata_file = dynamic_cast<NitroFileMetadata const*>(metadata_node);
    if (metadata_file == nullptr)
    {
        qDebug() << "Tried to extract file for which there is no metadata " << file_pointer;
        return false;
    }

    try {
        const auto bytes = file_pointer->Extract_File(nds_file, metadata_file->compression_type);
        const QString destination_file_path { directory_info.filePath(file_pointer->name) };
        QFile destination_file { destination_file_path };
        destination_file.open(QFile::WriteOnly);
        destination_file.write(bytes);
        return true;
    }
    catch (Failed_Extraction_Exception) {
        qDebug() << "Failed extraction for file " << file_pointer->Get_Node_Path();
        return false;
    }
}

std::set<NitroFile *> TreeModel::Extract_Directory(QString const& base_path, QFileDevice& nds_file, const NitroDirectory * const directory_pointer) const noexcept
{
    set<NitroFile*> files_unextracted{};
    const QDir parent_directory_info { base_path };
    QString effective_directory_name { directory_pointer->name };
    if ( dynamic_cast<const NitroNARC*>(directory_pointer) != nullptr )
        effective_directory_name += "_contents";    //The NARC Archive will be extracted as both a file and a directory, so we need to make sure these names differ.
    const QDir directory_info { parent_directory_info.filePath(effective_directory_name) };
    directory_info.mkpath(".");   //Ensure the directory exists.

    auto extract_directory = [=, &nds_file, &files_unextracted](auto* ptr_dir) {
        const set<NitroFile*> files_error_dir = this->Extract_Directory(directory_info.path(), nds_file, ptr_dir);
        files_unextracted = Merge_Sets(files_unextracted, files_error_dir);
    };

    for (unique_ptr<NitroDirectory> const& subdir : directory_pointer->children_directories)
    {
        extract_directory(subdir.get());
    }
    for (unique_ptr<NitroFile> const& file : directory_pointer->children_files)
    {
        if (!this->Extract_File(directory_info.path(), nds_file, file.get()))
        {
            files_unextracted.emplace(file.get());
        }
        NitroNARC* const maybe_narc_file { dynamic_cast<NitroNARC*>(file.get()) };
        if (maybe_narc_file != nullptr)
            extract_directory(static_cast<NitroDirectory*>(maybe_narc_file));
    }
    return files_unextracted;
}

QString TreeModel::Tree_View_Selection_Changed(const QModelIndexList &selected_indexes)
{
    if (selected_indexes.size() == 0)
        return tr("No selection");
    else if (selected_indexes.size() == 1)
    {
        const QModelIndex unique_index = selected_indexes[0];
        const NitroNode* const node_pointer { reinterpret_cast<const NitroNode*>(unique_index.internalPointer()) };
        const NitroDirectory* const directory_pointer { dynamic_cast<const NitroDirectory*>(node_pointer) };
        if (directory_pointer != nullptr)
            return Get_Info_String_for_Directory(directory_pointer);
        else
        {
            const NitroFile* const file_pointer { dynamic_cast<const NitroFile*>(node_pointer) };
            if (file_pointer != nullptr)
                return Get_Info_String_for_File(file_pointer);
            else return tr("Invalid selection");
        }
    }
    else
    {
        return Get_Info_String_for_Several_Selections(selected_indexes);
    }
}

bool TreeModel::Single_File_is_Selected(const QModelIndexList &selected_indexes)
{
    if (selected_indexes.size() != 1)
        return false;

    const QModelIndex index { selected_indexes[0] };
    const NitroNode* const node_pointer { reinterpret_cast<const NitroNode*>(index.internalPointer()) };
    if (node_pointer != nullptr && dynamic_cast<const NitroFile*>(node_pointer) != nullptr)
        return true;
    return false;
}

NitroFile *TreeModel::Get_Selected_File(const QModelIndex &index)
{
    NitroNode* const node_pointer { reinterpret_cast<NitroNode*>(index.internalPointer()) };
    if (node_pointer == nullptr)
        return nullptr;

    NitroFile* const nitro_file { dynamic_cast<NitroFile*>(node_pointer) };
    return nitro_file; //Either nullptr if node_pointer was not a NitroFile, or a NitroFile object.
}

set<NitroFile *> TreeModel::Extract_Indexes(QString const& base_path, QFileDevice &nds_file, const QModelIndexList &indexes)
{
    set<NitroFile*> files_extraction_error {};
    for (const auto& index : indexes)
    {
        NitroNode* const node_pointer { reinterpret_cast<NitroNode*>(index.internalPointer()) };
        NitroDirectory* const dir_pointer { dynamic_cast<NitroDirectory*>(node_pointer) };
        if (dir_pointer != nullptr)
        {
            const set<NitroFile*> files_error_dir = this->Extract_Directory(base_path, nds_file, dir_pointer);
            files_extraction_error = Merge_Sets(files_extraction_error, files_error_dir);
            NitroNARC* const narc_pointer { dynamic_cast<NitroNARC*>(node_pointer) };
            if (narc_pointer == nullptr)
            {
                continue;  //It's just a directory, therefore no need to also extract the NARC file.
            }
        }
        NitroFile* const file_pointer { dynamic_cast<NitroFile*>(node_pointer) };
        if (file_pointer != nullptr)
        {
            if (!this->Extract_File(base_path, nds_file, file_pointer))
            {
                files_extraction_error.emplace(file_pointer);
            }
        }
        else
        {
            qDebug() << "Invalid index with unknown pointer type: " << index;
        }
    }
    return files_extraction_error;
}

void TreeModel::Start_File_Type_Replacing(const QModelIndex & index)
{
    beginRemoveRows(index.parent(), index.row(), index.row());
    beginInsertRows(index.parent(), index.row(), index.row());
}

void TreeModel::End_File_Type_Replacing()
{
    endRemoveRows();
    endInsertRows();
}

void TreeModel::Change_Compression_Type_to_Raw(const NitroFile * const nitrofile)
{
    NitroNodeMetadata* const metadata_node { Find_Equivalent_Metadata_Node(nitrofile) };
    NitroFileMetadata* const metadata_file { dynamic_cast<NitroFileMetadata*>(metadata_node) };
    if (metadata_file == nullptr)
    {
        qDebug() << "Tried to change compression type of nullptr : nitrofile " << nitrofile << " - metadata_node " << metadata_node << " - metadata_file " << metadata_file;
        return;
    }

    metadata_file->compression_type = CompressionType::Raw;
}

NitroDirectoryMetadata* Find_Child_Directory_from_id(const NitroDirectoryMetadata* const parent, const quint16 id_child_directory)
{
    auto iter = find_if(parent->children_directories.begin(),
                        parent->children_directories.end(),
                        [id_child_directory](const auto& child_dir) {  return child_dir->id == id_child_directory; });
    if (iter != parent->children_directories.end())
        return (*iter).get();
    else return nullptr;
}

NitroNodeMetadata* Find_Child_Node_from_id(const NitroDirectoryMetadata* const parent, const quint16 child_id)
{
    auto same_node_id = [child_id](const auto& child_node) {
        return child_node->id == child_id;
    };

    const auto iter_dirs = find_if(parent->children_directories.begin(), parent->children_directories.end(),
                                   same_node_id);
    if (iter_dirs != parent->children_directories.end())
        return static_cast<NitroNodeMetadata*>((*iter_dirs).get());
    else
    {
        const auto iter_files = find_if(parent->children_files.begin(), parent->children_files.end(), same_node_id);
        if (iter_files != parent->children_files.end())
            return static_cast<NitroNodeMetadata*>((*iter_files).get());
        else return nullptr;
    }
}

NitroNodeMetadata* TreeModel::Find_Equivalent_Metadata_Node(const NitroNode * const nitrofs_node) const
{
    if (nitrofs_node == nullptr)
        return nullptr;
    if (nitrofs_node == static_cast<NitroNode*>(this->root_directory_nitro.get()))
        return static_cast<NitroNodeMetadata*>(this->root_directory_metadata.get());

    /* General strategy: construct a path from the root to this node by repeatedly collecting the
     * ids of the ancestors; use this list of ids in the metadata hierarchy to find the equivalent metadata node */

    vector<quint16> ids_of_ancestors {};
    const auto* ancestor = nitrofs_node->parent;
    while (ancestor != this->root_directory_nitro.get())
    {
        ids_of_ancestors.push_back(ancestor->id);
        ancestor = ancestor->parent;
    }

    //Now we go through the same path in reverse order to find the equivalent metadata node.
    reverse(ids_of_ancestors.begin(), ids_of_ancestors.end());
    const NitroDirectoryMetadata* dir_metadata_ancestor { this->root_directory_metadata.get() };
    auto iter_ids = ids_of_ancestors.begin();
    //We through each directory, one at a time, then figure out where the next child directory is.
    while (iter_ids != ids_of_ancestors.end())
    {
        const quint16 id { *iter_ids };
        dir_metadata_ancestor = dynamic_cast<NitroDirectoryMetadata const*>(Find_Child_Node_from_id(dir_metadata_ancestor, id));
        if (dir_metadata_ancestor == nullptr)
            return nullptr;   //Unless there's a contradiction in the hierarchy of the FS and that of the metadata, this return *should* never happen.
        iter_ids++;
    }

    //We should have found the parent directory metadata for the equivalent metadata node.
    //Now we just need to locate precisely the metadata node.
    return Find_Child_Node_from_id(dir_metadata_ancestor, nitrofs_node->id);
}
