#ifndef HEXEDITORDIALOG_HPP
#define HEXEDITORDIALOG_HPP

#include <QDialog>
#include <QByteArray>

namespace Ui {
class HexEditorDialog;
}

class HexEditorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HexEditorDialog(QByteArray&& bytes_file, QString const& file_path, QWidget *parent = 0);
    ~HexEditorDialog();

private:
    Ui::HexEditorDialog *ui;
    QByteArray bytes_file {};
};

#endif // HEXEDITORDIALOG_HPP
