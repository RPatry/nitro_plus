#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <QMainWindow>
#include "treemodel.hpp"
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString current_nds_file {};
    std::unique_ptr<TreeModel> tree_model {};

    template<typename NitroFileIter>
    /*!
     * \brief Negotiate_for_Failed_Extractions For any file that could not be extracted because of a bad compression format,
     * ask the user if he would like to change it to Raw instead.
     * \param begin An iterator to a range of NitroFile* that could not be extracted
     * \param end The range end
     */
    void Negotiate_for_Failed_Extractions(NitroFileIter begin, NitroFileIter end);

private slots:
    void OpenROMDialogue(void);

    void Setup_Connections(void);
    void Show_Error_Dialog(QString const&error_message);
    void Tree_View_Selection_Changed(QItemSelection const& new_items, QItemSelection const& old_items);
    void Extract_Selection(void);
    void Reinsert_File(void);
    void Show_About_Dialog(void);
    void Show_Info_Dialog(void);
    void Show_Hex_Editor(void);

};

#endif // MAIN_WINDOW_HPP
