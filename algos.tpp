#include <set>
#include <algorithm>

template<typename Key>
std::set<Key> Merge_Sets(std::set<Key> const& first_set, std::set<Key> const& second_set)
{
    std::set<Key> result{};
    std::merge(first_set.begin(), first_set.end(),
               second_set.begin(), second_set.end(),
               std::inserter(result, result.begin()));
    return result;
}
