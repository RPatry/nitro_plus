#include "filedatadialog.hpp"
#include "ui_filedatadialog.h"
#include "metadata.hpp"
#include <QStringListModel>
#include <QLocale>
#include <vector>

using namespace std;

FileDataDialog::FileDataDialog(QWidget *parent, const NitroFile * const file, CompressionType comp_type, QByteArray &&bytes_file) :
    QDialog(parent),
    ui(new Ui::FileDataDialog), stream_file{std::move(bytes_file)}
{
    ui->setupUi(this);

    //Get rid of "?" button
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    this->Setup_Connections();

    QLocale system_locale{};
    this->ui->label_Begin_Offset->setText(QString{tr("0x%1")}.arg(QString::number(file->start_offset, 16).toUpper()));
    this->ui->label_End_Offset->setText(QString{tr("0x%1")}.arg(QString::number(file->end_offset, 16).toUpper()));
    this->ui->label_Size->setText(QString{tr("%1 bytes")}.arg(system_locale.toString(file->end_offset - file->start_offset)));

    QStringList model{};
    model << tr("Raw") << tr("LZ77");
    ui->comboBox_Compression_Type->addItems(model);
    ui->comboBox_Compression_Type->setCurrentIndex(static_cast<int>(comp_type));
    this->setWindowTitle(this->windowTitle() + ' ' + file->Get_Node_Path());
}

FileDataDialog::~FileDataDialog()
{
    delete ui;
}

CompressionType FileDataDialog::Get_Compression_Type() const noexcept
{
    return static_cast<CompressionType>(this->ui->comboBox_Compression_Type->currentIndex());
}

void FileDataDialog::Setup_Connections() noexcept
{
    //Had already solved this in February 2015; copied straight from there.
    typedef void (QComboBox::*Overload)(int);     //This trick with function pointers allows Qt to determine the correct overload for currentIndexChanged.
    Overload over = &QComboBox::currentIndexChanged;
    connect(ui->comboBox_Compression_Type, over, this, &FileDataDialog::Update_Compression_Explanation);
    connect(ui->pushButton_Guess_Compression_Type, &QPushButton::clicked, this, &FileDataDialog::Guess_Compression_Type);
}

void FileDataDialog::Update_Compression_Explanation(int index) noexcept
{
    const vector<QString> possible_strings {
        tr("This file will be extracted as is and reinserted as is."),
        tr("This file will be decompressed using LZ77 before extraction, and it will be compressed using LZ77 before reinsertion."),
        tr("Invalid compression type!")
    };
    if (index < 0 || index >= static_cast<int>(possible_strings.size()))
        index = possible_strings.size() - 1;    //Point to the error string.

    this->ui->label_Explanation->setText(possible_strings[index]);
}

void FileDataDialog::Guess_Compression_Type() noexcept
{
    CompressionType type { ::Guess_Compression_Type(this->stream_file) };
    ui->comboBox_Compression_Type->setCurrentIndex(static_cast<int>(type));
}
