#ifndef ABOUTNITRODIALOG_HPP
#define ABOUTNITRODIALOG_HPP

#include <QDialog>

namespace Ui {
class AboutNitroDialog;
}

class AboutNitroDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutNitroDialog(QWidget *parent = 0);
    ~AboutNitroDialog();

private:
    Ui::AboutNitroDialog *ui;
};

#endif // ABOUTNITRODIALOG_HPP
