#include "hexeditordialog.hpp"
#include "ui_hexeditordialog.h"
#include "qhexedit2/src/qhexedit.h"
#include <vector>

using namespace std;

HexEditorDialog::HexEditorDialog(QByteArray &&_bytes_file, const QString &file_path, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HexEditorDialog), bytes_file{std::move(_bytes_file)}
{
    ui->setupUi(this);
    QHexEdit* const hex_edit { new QHexEdit{this} };
    ui->horizontalLayout_container_Hex_Edit->addWidget(hex_edit);
    hex_edit->setData(bytes_file);
    //Get rid of "?" button
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowTitle(QString{tr("Viewing %1")}.arg(file_path));
    showMaximized();
}

HexEditorDialog::~HexEditorDialog()
{
    delete ui;
}
