#include "metadata.hpp"
#include <set>
#include <iterator>
#include <algorithm>
#include <QBuffer>

using namespace std;

bool Check_Same_Children_Files(const NitroDirectoryMetadata* const directory_metadata,
                               const NitroDirectory* const directory)
{
    //Gather all the ids for children files, in both the filesystem data and the metadata, and check if they are equal
    set<quint16> ids_files_metadata {};
    set<quint16> ids_files_directory {};
    auto get_file_id = [](const auto& file) { return file->id; };
    //Might be too convoluted? Basically extracts the ids of children nodes by iterating over them.
    transform(directory_metadata->children_files.begin(), directory_metadata->children_files.end(),
              inserter(ids_files_metadata, ids_files_metadata.begin()),
              get_file_id);
    transform(directory->children_files.begin(), directory->children_files.end(),
              inserter(ids_files_directory, ids_files_directory.begin()),
              get_file_id);
    return ids_files_directory == ids_files_metadata;
}

/*!
 * \brief Try to locate a child directory's metadata by looking for its id.
 * \return nullptr if not found; the metadata otherwise (obviously)
 */
const NitroDirectoryMetadata* Find_Child_Metadata(const NitroDirectoryMetadata* const parent, quint16 id_child_directory)
{
    const auto iter_dir_metadata = find_if(parent->children_directories.begin(),
                                        parent->children_directories.end(),
                                           [id_child_directory](const auto& subdir) {
        return subdir->id == id_child_directory;
    });
    if (iter_dir_metadata == parent->children_directories.end())
        return nullptr;
    else
        return (*iter_dir_metadata).get();
}

bool Trees_are_Equal(const NitroDirectoryMetadata * const directory_metadata, const NitroDirectory * const directory)
{
    if (directory->id != directory_metadata->id)
        return false;

    for (const auto& subdir : directory->children_directories)
    {
        const auto* equivalent_metadata = Find_Child_Metadata(directory_metadata, subdir->id);
        if (equivalent_metadata == nullptr)   //In the metadata, the directory does not exist.
            return false;
        else if (!Trees_are_Equal(equivalent_metadata, subdir.get()))
            return false;
    }
    return Check_Same_Children_Files(directory_metadata, directory);
}

CompressionType Guess_Compression_Type(const MemoryStream &nds_stream, const NitroFile * const file)
{
    const auto first_byte = nds_stream.Read_U8(file->start_offset);
    if (first_byte == 0x10)
    {
        return CompressionType::LZ77;
    }
    return CompressionType::Raw;
}

CompressionType Guess_Compression_Type(const MemoryStream &file_stream)
{
    const auto first_byte = file_stream.Read_U8(0);
    if (first_byte == 0x10)
    {
        return CompressionType::LZ77;
    }
    return CompressionType::Raw;

}

template<typename NitroDirMetadataType>
std::unique_ptr<NitroDirMetadataType> Parse_File_System_Guess_Metadata(MemoryStream &nds_or_narc_stream, NitroDirectory * const directory,
                                                                    NitroDirectoryMetadata* const parent)
{
    auto dir_metadata = make_unique<NitroDirMetadataType>(directory->id, parent);
    for (const auto& child_dir : directory->children_directories)
    {
        dir_metadata->children_directories.push_back(
                    Parse_File_System_Guess_Metadata<NitroDirectoryMetadata>
                    (nds_or_narc_stream, child_dir.get(), dir_metadata.get()));
    }
    for (const auto& child_file : directory->children_files)
    {
        auto file_metadata = make_unique<NitroFileMetadata>(child_file->id, dir_metadata.get());
        file_metadata->compression_type = Guess_Compression_Type(nds_or_narc_stream, child_file.get());
        auto* ptr_metadata = file_metadata.get();
        dir_metadata->children_files.push_back(move(file_metadata));
        Check_for_NARC_with_Current_Compression_Type(nds_or_narc_stream, child_file.get(), ptr_metadata);
    }

    return move(dir_metadata);
}

// Manual instantiations
template std::unique_ptr<NitroDirectoryMetadata> Parse_File_System_Guess_Metadata(MemoryStream &nds_stream, NitroDirectory * const directory,
                                                                    NitroDirectoryMetadata* const parent);

template std::unique_ptr<NitroNARCMetadata> Parse_File_System_Guess_Metadata(MemoryStream &nds_stream, NitroDirectory * const directory,
                                                                    NitroDirectoryMetadata* const parent);

unique_ptr<NitroDirectoryMetadata> Get_Metadata_for_ROM(const QString &rom_name_from_header, const QString &path_rom)
{
    Q_UNUSED(rom_name_from_header);
    Q_UNUSED(path_rom);
    return unique_ptr<NitroDirectoryMetadata>{};
}

NitroDirectoryMetadata *NitroNodeMetadata::Find_Parent_Node_with_Id(quint16 id_parent) const
{
    if (this->parent == nullptr)
        return nullptr;
    else if (this->parent->id == id_parent)
        return this->parent;
    else return this->parent->Find_Parent_Node_with_Id(id_parent);
}
