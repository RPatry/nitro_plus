#ifndef ALGOS_HPP
#define ALGOS_HPP

#include <set>
#include <algorithm>

template<typename Key>
std::set<Key> Merge_Sets(std::set<Key> const& first_set, std::set<Key> const& second_set);

#include "algos.tpp"

#endif // ALGOS_HPP
