#ifndef FILEDATADIALOG_HPP
#define FILEDATADIALOG_HPP

#include <QDialog>
#include "memorystream.hpp"
#include "metadata.hpp"
#include "nitrofs.hpp"

namespace Ui {
class FileDataDialog;
}

class FileDataDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileDataDialog(QWidget *parent, const NitroFile* const file, CompressionType comp_type, QByteArray&& bytes_file);
    ~FileDataDialog();

    CompressionType Get_Compression_Type(void) const noexcept;

private:
    Ui::FileDataDialog *ui;
    MemoryStream stream_file;

    void Setup_Connections(void) noexcept;

private slots:
    void Update_Compression_Explanation(int index) noexcept;
    void Guess_Compression_Type(void) noexcept;
};

#endif // FILEDATADIALOG_HPP
