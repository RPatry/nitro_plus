#include "nitrofs.hpp"
#include "metadata.hpp"
#include "lz77.hpp"
#include <QFile>
#include <QDebug>
#include <QBuffer>
#include <QDataStream>
#include <algorithm>
#include <vector>

using namespace std;

NDSHeader::NDSHeader(const MemoryStream &nds_rom) :
    game_title { nds_rom.Read_String(0, 12) },
    game_code { nds_rom.Read_String(0xC, 4) },
    maker_code { nds_rom.Read_String(0x10, 2) },
    rom_version { nds_rom.Read_U8(0x1E) },
    arm9_rom_offset { nds_rom.Read_U32(0x20) },
    arm9_entry_address { nds_rom.Read_U32(0x24) },
    arm9_ram_address { nds_rom.Read_U32(0x28) },
    arm7_rom_offset { nds_rom.Read_U32(0x30) },
    arm7_entry_address { nds_rom.Read_U32(0x34) },
    arm7_ram_address { nds_rom.Read_U32(0x38) },
    fnt_offset { nds_rom.Read_U32(0x40) },
    fnt_size { nds_rom.Read_U32(0x44) },
    fat_offset { nds_rom.Read_U32(0x48) },
    fat_size { nds_rom.Read_U32(0x4C) },
    icon_title_offset { nds_rom.Read_U32(0x68) }
{}

//Parse a directory sub-entry in the directory table of the FNT.
//Returns an object corresponding to the directory with all of its children, but without the name of the directory
//(because this information is not accessible in the directory table, it will fall upon a parent to name its child directory).
//This function is called recursively for subdirectories.
template<typename NitroDirType>    //Either NitroDirectory or NitroNARC
unique_ptr<NitroDirType> Parse_Nitro_Directory(const quint32 fnt_offset, const quint32 fat_offset,
                                                 const quint16 directory_id, MemoryStream const& nds_rom,
                                                 NitroDirectory* const parent, QString const& directory_name,
                                               bool is_root_directory)
{
    const quint16 effective_id_for_maintable { is_root_directory? static_cast<quint16>(0xF000) : directory_id };
    const quint32 directory_maintable_offset { fnt_offset + (effective_id_for_maintable - 0xF000) * 8 };
    const quint32 subtable_offset { nds_rom.Read_U32(directory_maintable_offset) };
    const quint32 directory_subtable_offset { subtable_offset + fnt_offset };
    const quint16 id_first_file { nds_rom.Read_U16(directory_maintable_offset + 4) };

    quint32 pointer_byte { directory_subtable_offset };   //Steadily incremented to read data from the directory subtable
    auto directory = make_unique<NitroDirType>(directory_id, directory_name, parent);

    quint16 id_new_file { id_first_file };
    quint8 length_subentry { nds_rom.Read_U8(pointer_byte) };

    while (length_subentry != 0)  //0x00 is the end delimiter for the directory
    {
        pointer_byte++;     //Pointing to the first byte of the name now
        const auto length_name = (length_subentry & 0x7F);
        const QString name_subentry { nds_rom.Read_String(pointer_byte, length_name) };
        const bool subentry_is_directory { (length_subentry & 0x80) > 0 };
        pointer_byte += length_name;   //Pointing after the name now (to the Directory ID if this is a Directory)
        if (subentry_is_directory)
        {
            const quint16 subdirectory_id { nds_rom.Read_U16(pointer_byte) };
            pointer_byte += 2;   //Skip past directory ID, point to next entry length
            auto subdirectory = Parse_Nitro_Directory<NitroDirectory>(fnt_offset, fat_offset, subdirectory_id, nds_rom, directory.get(), name_subentry, false);
            directory->children_directories.push_back(std::move(subdirectory));
        }
        else
        {
            const quint32 fat_offset_for_file { fat_offset + 8 * id_new_file };
            const quint32 begin_offset { nds_rom.Read_U32(fat_offset_for_file) };
            const quint32 end_offset { nds_rom.Read_U32(fat_offset_for_file + 4) };
            auto new_file = make_unique<NitroFile>(id_new_file, name_subentry, directory.get(), begin_offset, end_offset);
            directory->children_files.push_back(std::move(new_file));
            id_new_file++;
        }

        length_subentry = nds_rom.Read_U8(pointer_byte);
    }

    return directory;
}

unique_ptr<NitroDirectory> Parse_ROM_for_NitroFS(const MemoryStream &nds_rom)
{
    const NDSHeader header { nds_rom };
    auto root_directory = Parse_Nitro_Directory<NitroDirectory>(header.fnt_offset, header.fat_offset,
                                                0xF000, nds_rom, nullptr, "/", true);

    return root_directory;
}

NitroDirectory::~NitroDirectory()
{

}

void NitroDirectory::Move_Children_Files_Offsets(const quint32 old_end_offset_updated_file, const qint32 diff_offset)
{
    for (const auto& subfile : this->children_files)
    {
        if (subfile->start_offset > old_end_offset_updated_file)
        {
            subfile->start_offset += diff_offset;
            subfile->end_offset += diff_offset;
        }
    }
    /* Because child NARC Archives are stored in children_files, we will not mess up their own file systems.
    / (Remember that if a file is reinserted in the NDS ROM/NARC Archive to which this directory belongs,
    / then only the files of the NDS ROM/NARC Archive must be affected, not those of children NARC Archives). */
    for (const auto& subdir : this->children_directories)
    {
        subdir->Move_Children_Files_Offsets(old_end_offset_updated_file, diff_offset);
    }
}

std::pair<size_t, size_t> NitroDirectory::Count_Directories_and_Files() const noexcept
{
    const size_t number_dirs { this->children_directories.size() };
    const size_t number_files { this->children_files.size() };
    return std::accumulate(this->children_directories.begin(),
                           this->children_directories.end(),
                           std::pair<size_t, size_t>{number_dirs, number_files},
                           [this](const std::pair<size_t, size_t> pair_dirs_files, std::unique_ptr<NitroDirectory> const& dir)
    {
        const auto count_subdir = dir->Count_Directories_and_Files();
        return std::pair<size_t, size_t>{pair_dirs_files.first + count_subdir.first,
                    pair_dirs_files.second + count_subdir.second};
    });
}

quint16 NitroDirectory::Get_Max_File_ID() const noexcept
{
    if (this->children_directories.size() == 0 && this->children_files.size() == 0)
        return 0;

    quint16 max_id {};
    //We only look recursively through children_directories, and children_files store the NARC Archives
    //if any, so this algorithm works correctly (we obviously don't want to mix it up with IDs from inside NARC Archives!)
    for (const auto& subdir : this->children_directories)
    {
        max_id = std::max(max_id, subdir->Get_Max_File_ID());
    }
    for (const auto& subfile : this->children_files)
    {
        max_id = std::max(subfile->id, max_id);
    }
    return max_id;
}

void Throw_if_NARC_has_bad_header(MemoryStream const& narc_file)
{
    //Some perhaps superficial tests to get rid of potentially faulty NARC files
    if (narc_file.Read_String(0, 4) != "NARC")
    {
        qDebug() << "NARC parsing - bad magic string";
        throw NARC_Parsing_Failure{};
    }
    if (narc_file.Read_U16(4) != 0xFFFE)
    {
        qDebug() << "NARC parsing - bad byte order";
        throw NARC_Parsing_Failure{};
    }
    if (narc_file.Read_U16(6) != 0x0100)
    {
        qDebug() << "NARC parsing - bad version number";
        throw NARC_Parsing_Failure{};
    }
    if (narc_file.Read_U16(0xC) != 0x0010)
    {
        qDebug() << "NARC parsing - bad header size";
        throw NARC_Parsing_Failure{};
    }
    if (narc_file.Read_U16(0xE) != 0x0003)
    {
        qDebug() << "NARC parsing - bad chunk number";
        throw NARC_Parsing_Failure{};
    }

}

/*!
 * When reading the contents of the NARC file, the file offsets that are read are not absolute within the file;
 * they're defined compared to the start of the FIMG section. When a NARC Archive is decrypted, the offset
 * must be computed and then passed to this function.
 */
template<typename NitroDirType>
void Update_NARC_File_Offsets(std::unique_ptr<NitroDirType> const& narc_root, const quint32 offset)
{
    for (auto& subdir : narc_root->children_directories)
    {
        Update_NARC_File_Offsets(subdir, offset);
    }
    for (auto& subfile : narc_root->children_files)
    {
        subfile->start_offset += offset;
        subfile->end_offset += offset;
    }
}

NARCHeader::NARCHeader(const MemoryStream &narc_archive) :
    number_of_files { narc_archive.Read_U32(0x18) }, fat_offset { 0x1C },
    fat_size { number_of_files * 8 },
    fnt_offset { fat_offset + number_of_files * 8 + 8 },
    fnt_size { narc_archive.Read_U32(fnt_offset - 4) },   //Yes, that - 4 is not an error
    fimg_offset { fnt_offset + fnt_size - 8 },
    fimg_size { narc_archive.Read_U32(fimg_offset + 4) }  //Again, - 8, not a mistake
{   }

std::unique_ptr<NitroNARC> Parse_NARC_for_NitroFS(const MemoryStream &narc_file, NitroDirectory* parent, quint16 id, QString const name,
                                                  quint32 start_offset, quint32 end_offset)
{
    Throw_if_NARC_has_bad_header(narc_file);

    const NARCHeader header { narc_file };

    auto narc_node = Parse_Nitro_Directory<NitroNARC>(header.fnt_offset, header.fat_offset, id, narc_file, parent, name, true);
    narc_node->start_offset = start_offset;
    narc_node->end_offset = end_offset;
    narc_node->archive_contents = narc_file.Get_All_Bytes();
    Update_NARC_File_Offsets(narc_node, header.fimg_offset + 8);
    return std::move(narc_node);
}

NitroNode::~NitroNode()
{

}

QString NitroNode::Get_Node_Path() const noexcept
{
    if (parent == nullptr)
        return "";    //We're at the root; no path to the root.
    else
    {
        QString parent_path { parent->Get_Node_Path() };
        parent_path.append('/');
        parent_path.append(this->name);
        return move(parent_path);
    }
}

NitroNARC *NitroNode::Get_Ancestor_NARC() const noexcept
{
    if (this->parent == nullptr)
        return nullptr;
    NitroNARC* maybe_narc_parent { dynamic_cast<NitroNARC*>(this->parent) };
    if (maybe_narc_parent != nullptr)
        return maybe_narc_parent;
    else
        return this->parent->Get_Ancestor_NARC();   //The parent is not null, but it's also not a NARC Archive. Keep looking then.
}

NitroFile::~NitroFile()
{

}

QByteArray NitroFile::Extract_File(QIODevice &nds_file, CompressionType type) const
{
    /* Two things: we could be extracting a file from the Nitro FS of a DS ROM, in which
     * case we'll use the nds_file parameter directly, or we could be extracting from a NARC archive
     * in which case we must find it and use its bytes instead. */
    QBuffer buffer_narc_archive {};   //Unused if we extract from an NDS file
    QIODevice* file_to_extract_from {}; //could be an NDS ROM or a NARC File.
    NitroNARC* maybe_narc { this->Get_Ancestor_NARC() };
    if (maybe_narc != nullptr)
    {
        buffer_narc_archive.setBuffer(&maybe_narc->archive_contents);  //buffer_narc_archive does not own the archive contents.
        buffer_narc_archive.open(QBuffer::ReadOnly);
        file_to_extract_from = &buffer_narc_archive;
    }
    else
    {
        file_to_extract_from = &nds_file;
    }
    if (!file_to_extract_from->seek(this->start_offset))
        throw Failed_Extraction_Exception{};

    const auto this_file_size = this->end_offset - this->start_offset;
    try {
        switch (type)
        {
        case CompressionType::LZ77:
        {
            QByteArray buffer { file_to_extract_from->read(this_file_size) };
            const auto decompressed = lz77::decompress(buffer.begin(), buffer.end());
            if (decompressed.size() == 0)
                throw Failed_Extraction_Exception{};
            QByteArray return_array(decompressed.data(), decompressed.size());
            return return_array;
        }
        break;
        case CompressionType::Raw:
        {
            return file_to_extract_from->read(this_file_size);
        }
        break;
        default:
            return QByteArray{};
        }
    }
    catch (...)
    {
        throw Failed_Extraction_Exception{};
    }
    return QByteArray{};
}

QByteArray NitroFile::Extract_File(MemoryStream &stream_nds_file, CompressionType type) const
{
    QBuffer buffer{&stream_nds_file.Get_All_Bytes()};    //buffer doesn't own the bytes inside of stream_nds_file
    buffer.open(QBuffer::ReadOnly);
    return this->Extract_File(buffer, type);
}

QByteArray NitroFile::Extract_Raw_File(QIODevice &nds_file) const
{
    nds_file.seek(this->start_offset);
    NitroNARC* maybe_ancestor_narc { this->Get_Ancestor_NARC() };
    if (maybe_ancestor_narc == nullptr)
    {
        QByteArray result { nds_file.read(this->end_offset - this->start_offset) };
        return result;
    }
    else
    {
        return maybe_ancestor_narc->archive_contents.mid(this->start_offset, this->end_offset - this->start_offset);
    }
}

void NitroFile::Reinsert_File(QFile &nds_file, const QByteArray &new_file_contents, const NitroFileMetadata *file_metadata)
{
    NitroNARC* const maybe_narc_ancestor { this->Get_Ancestor_NARC() };
    if (maybe_narc_ancestor != nullptr)
    {
        //Locate the metadata for the NARC ancestor; we need to know its compression type so that it too can be reinserted in its parent.
        NitroDirectoryMetadata const* const parent_meta { file_metadata->Find_Parent_Node_with_Id(maybe_narc_ancestor->id) };
        NitroNARCMetadata const* narc_metadata { dynamic_cast<NitroNARCMetadata const*>(parent_meta) };
        if (narc_metadata == nullptr)
        {
            qDebug() << "Could not find equivalent parent NARC metadata when reinserting file!";
            return;
        }

        //We need to reinsert this file inside the NARC archive, then reinsert the NARC Archive in the NDS ROM
        //With compressing the whole thing as needed, of course
        Reinsert_Child_File_and_Rebuild_NARC(maybe_narc_ancestor, this, new_file_contents, file_metadata);
        //Reverse-recursive call
        maybe_narc_ancestor->Reinsert_File(nds_file, maybe_narc_ancestor->archive_contents, narc_metadata);
    }
    else   //Base case for recursive call, we reached the Nintendo DS ROM File system.
    {
        //No NARC archive in the ancestors; then it belongs directly to a NDS File; update the NitroROM
        //file system as needed.
        Reinsert_Child_File_and_Rebuild_NitroFS(nds_file, this, new_file_contents, file_metadata);
    }
}

NitroNARC::~NitroNARC()
{

}

/*!
 * \brief Read_NDS_Icon_Palette Extract the palette for the icon of the ROM
 * \param nds_rom
 * \param icon_title_offset The offset at which the icon and palette area can be found inside the ROM
 * \return An empty vector if there was an error; otherwise a list of 16 colors.
 */
vector<QRgb> Read_NDS_Icon_Palette(const MemoryStream& nds_rom, const quint32 icon_title_offset)
{
    vector<QRgb> palette {};
    if (icon_title_offset > 0)
    {
        const auto base_palette_offset = icon_title_offset + 0x220;
        for (size_t i = 0; i < 16; i++)
        {
            const auto current_color_offset = base_palette_offset + i * 2;
            const quint16 color { static_cast<quint16>(nds_rom.Read_U16(static_cast<int>(current_color_offset)) & 0x7FFF) };
            const int r { color & 0x1F }, g { (color >> 5) & 0x1F }, b { (color >> 10) & 0x1F };
            palette.push_back(QColor{(r << 3), (g << 3), (b << 3)}.rgb());
        }
    }
    return palette;
}

QIcon Get_NDS_ROM_Icon(const MemoryStream &nds_rom)
{
    /* For reference: https://www.akkit.org/info/gbatek.htm#dscartridgeicontitle */

    const int width_icon { 32 }, height_icon { 32 };
    QImage image{width_icon, height_icon, QImage::Format_RGB555 };
    const NDSHeader header { nds_rom };
    const auto palette = Read_NDS_Icon_Palette(nds_rom, header.icon_title_offset);
    if (palette.size() < 16)
        return QIcon{QPixmap{0, 0}};

    /* Calculations to convert from tile to a 32x32 picture.
    * The pixels are not stored linearly for the 32x32 picture, that's the principle of tiling.
    * For example, in a 16x16 picture, the first tile
    * is for top left 8x8 picture, second tile for top right, third for bottom left, fourth for bottom right.
    * And that's all there is to know. In a tile, the pixels of a 8x8 are stored linearly, row by row
    * (first four bytes for first row of pixels, and so on). Each nybble in a byte points to a color in the
    * palette: the color for a single pixel. */
    int offset_tile_byte { static_cast<int>(header.icon_title_offset + 0x20) };
    //Here tiles 0 to 3 correspond to the top row of tiles, and 12 to 15 to the bottom row of tiles
    for (size_t nb_tile = 0; nb_tile < 16; nb_tile++)
    {
        const size_t x_tile { nb_tile % 4 }, y_tile { nb_tile / 4 };   //In both cases a number € [0; 3]

        //Iterating over the pixels in a tile, two pixels by two (that's 8 rows of 4 pairs)
        for (size_t y = 0; y < 8; y++)
        {
            for (size_t x = 0; x < 8; x += 2)
            {
                const quint8 byte { nds_rom.Read_U8(offset_tile_byte) };
                const quint8 first_pixel { static_cast<quint8>(byte & 0xF) }, second_pixel { static_cast<quint8>((byte >> 4) & 0xF) };
                //Compute the actual position of the pixel in the 32x32 icon.
                const int x_first_pixel { static_cast<int>(x + x_tile * 8) },
                          y_pixels { static_cast<int>(y + y_tile * 8) };
                image.setPixel(x_first_pixel, y_pixels, palette[first_pixel]);
                image.setPixel(x_first_pixel + 1, y_pixels, palette[second_pixel]);

                offset_tile_byte++;
            }
        }
    }

    return QIcon{ QPixmap::fromImage(image) };
}

void Replace_with_NARC(const NitroFile* file_node, const NitroFileMetadata* metadata_node, std::unique_ptr<NitroNARC> new_node, std::unique_ptr<NitroNARCMetadata> new_metadata_node)
{
    auto* parent = file_node->parent;
    auto iter = find_if(parent->children_files.begin(), parent->children_files.end(),
                        [file_node](const auto& sub_file) {  return sub_file.get() == file_node; });
    if (iter != parent->children_files.end())
    {
        *iter = std::move(new_node);
    }
    //Now replace it in the metadata hierarchy too
    auto* metadata_parent = metadata_node->parent;
    auto metadata_iter = find_if(metadata_parent->children_files.begin(), metadata_parent->children_files.end(),
                                 [metadata_node](const auto& sub_metadata_file) { return sub_metadata_file.get() == metadata_node; });
    if (metadata_iter != metadata_parent->children_files.end())
    {
        *metadata_iter = std::move(new_metadata_node);
    }
}

void Replace_with_Guaranteed_File_Node(const NitroFile* file_node_but_maybe_narc, const NitroFileMetadata* file_metadata_maybe_narc)
{
    auto* parent = file_node_but_maybe_narc->parent;
    auto iter = find_if(parent->children_files.begin(), parent->children_files.end(),
                        [file_node_but_maybe_narc](const auto& sub_file)
    {  return sub_file.get() == file_node_but_maybe_narc; });
    if (iter != parent->children_files.end())
    {
        *iter = make_unique<NitroFile>(file_node_but_maybe_narc->id,
                                       file_node_but_maybe_narc->name,
                                       file_node_but_maybe_narc->parent,
                                       file_node_but_maybe_narc->start_offset,
                                       file_node_but_maybe_narc->end_offset);
    }
    auto* metadata_parent = file_metadata_maybe_narc->parent;
    auto iter_meta = find_if(metadata_parent->children_files.begin(), metadata_parent->children_files.end(),
                             [file_metadata_maybe_narc](const auto& subfile)
    {
        return subfile.get() == file_metadata_maybe_narc;
    });
    if (iter_meta != metadata_parent->children_files.end())
    {
        CompressionType type { file_metadata_maybe_narc->compression_type };  //Saving it before the metadata node is destroyed below
        *iter_meta = make_unique<NitroFileMetadata>(file_metadata_maybe_narc->id, metadata_parent);
        (*iter_meta)->compression_type = type;
    }
}

void Check_for_NARC_with_Current_Compression_Type(MemoryStream & stream_nds_or_narc_file, const NitroFile *file, const NitroFileMetadata *file_metadata)
{
    try {
        auto file_bytes = file->Extract_File(stream_nds_or_narc_file, file_metadata->compression_type);
        MemoryStream file_mem_stream{std::move(file_bytes)};
        if (file_mem_stream.Read_String(0, 4) == "NARC")
        {
            //We probably have a NARC file. Now try to parse it.
            std::unique_ptr<NitroNARC> root_narc_file = std::move(Parse_NARC_for_NitroFS(file_mem_stream, file->parent,
                                                                               file->id, file->name,
                                                                               file->start_offset, file->end_offset));
            std::unique_ptr<NitroNARCMetadata> root_metadata = std::move(Parse_File_System_Guess_Metadata<NitroNARCMetadata>
                    (file_mem_stream, root_narc_file.get(), file_metadata->parent));
            root_metadata->compression_type = file_metadata->compression_type;

            //And now we need to replace it in the parent
            Replace_with_NARC(file, file_metadata, std::move(root_narc_file), std::move(root_metadata));
        }
        else
        {
            Replace_with_Guaranteed_File_Node(file, file_metadata);
        }
    }
    catch (Failed_Extraction_Exception)
    {
        //We do nothing; extraction failed, so the compression type is obviously not suited
        //so it obviously can't be a NARC file with the current compression.
        Replace_with_Guaranteed_File_Node(file, file_metadata);
    }
    catch (NARC_Parsing_Failure) {
        //Or parsing the NARC file failed, but if that's the case, then it can't possibly be
        //a valid NARC file so we don't do anything either.
        qDebug() << file->Get_Node_Path() << " could not be successfully be parsed as a NARC file.";
        Replace_with_Guaranteed_File_Node(file, file_metadata);
    }
}

inline int Get_Number_Bytes_for_Padding(const int old_and_new_file_sizes_difference) noexcept
{
    const int imbalance { old_and_new_file_sizes_difference % 4 };
    return (4 - imbalance) % 4;
}

/* A generic file reinsertion algorithm which can work for both NDS ROMs and NARC Archives. Returns the
/  difference in bytes of the space occupied by the file before and after reinsertion (taking into account padding!) */
int Reinsert_Child_File(NitroDirectory* root_directory_for_fs, NitroFile* file_reinserted, QByteArray const& file_new_contents_compressed, QByteArray& byte_array_for_fs)
{
    //We insert the new file contents in the Nintendo DS ROM/NARC Archive
    const size_t old_file_length { file_reinserted->end_offset - file_reinserted->start_offset };
    byte_array_for_fs.replace(file_reinserted->start_offset, old_file_length, file_new_contents_compressed);

    if (file_reinserted->id == root_directory_for_fs->Get_Max_File_ID())
    {
        //We're editing the last file (for the File Allocation Table), so no need to add padding,
        //or to move any files that come behind since there aren't any.
        file_reinserted->end_offset = file_reinserted->start_offset + file_new_contents_compressed.size();
        return file_new_contents_compressed.size() - static_cast<int>(old_file_length);
    }

    int nb_bytes_padding {};
    //This file is not the last of the file system.
    if (old_file_length != static_cast<size_t>(file_new_contents_compressed.size()))
    {
        /* Add some padding if necessary; this way all the files after this one will still be at a word boundary
        / (we assume they were before reinsertion, so adding padding will maintain this invariant). */
        nb_bytes_padding = Get_Number_Bytes_for_Padding(file_new_contents_compressed.size() - static_cast<int>(old_file_length));
        const QByteArray padding {nb_bytes_padding, static_cast<char>(0xFF)};   //Padding with 0xFF seems to be some convention; I'm not sure if using any other value would cause problems
        byte_array_for_fs.insert(file_reinserted->start_offset + file_new_contents_compressed.size(), padding);
    }

    //Now, we inform our in-memory structures that some files have been moved.
    const int diff { file_new_contents_compressed.size() + nb_bytes_padding - static_cast<int>(old_file_length) };
    Q_ASSERT((diff % 4) == 0);  //Crucial: the files behind must move by a multiple of 4 so that they are still on a word boundary.
    if (diff != 0)
    {
        root_directory_for_fs->Move_Children_Files_Offsets(file_reinserted->end_offset, diff);
    }
    file_reinserted->end_offset = file_reinserted->start_offset + file_new_contents_compressed.size();

    return diff;
}

/* Essential in the process of rebuilding NARC Archives/NDS ROMs (when reinserting a file)
 * is the creation of a new File Allocation Table (FAT)
 * to inform the Nintendo DS where the files have been moved. This algorithm does just that.
 * Warning! The file reinserted does not have its offsets updated! The end offset must be updated manually.
 * The reason for that is that because of padding, the diff might be off a few bytes compared to the diff
 * for the files following it. */
QByteArray Build_New_File_Allocation_Table(QByteArray const& old_fat, const quint32 old_end_offset_of_file_reinserted, const int diff_offsets)
{
    const qint32 number_of_files { old_fat.size() / 8 };   //8 bytes per file: 4 for start address, 4 for the end address
    QByteArray new_fat {};

    QDataStream old_fat_stream { old_fat };
    old_fat_stream.setByteOrder(QDataStream::LittleEndian);
    QDataStream new_fat_stream { &new_fat, QIODevice::WriteOnly };
    new_fat_stream.setByteOrder(QDataStream::LittleEndian);

    //For each file, we check if it is situated after the file that was reinserted. If it was, update its pointers.
    for (qint32 i = 0; i < number_of_files; i++)
    {
        quint32 begin_offset{}, end_offset{};
        old_fat_stream >> begin_offset >> end_offset;
        if (begin_offset > old_end_offset_of_file_reinserted)
        {
            begin_offset += diff_offsets;
            end_offset += diff_offsets;
        }
        new_fat_stream << begin_offset << end_offset;
    }

    return new_fat;
}

void Reinsert_Child_File_and_Rebuild_NARC(NitroNARC *narc_file, NitroFile *descendant_of_narc, const QByteArray &file_new_contents, NitroFileMetadata const *file_metadata)
{
    Q_ASSERT(narc_file == descendant_of_narc->Get_Ancestor_NARC());
    const auto old_end_offset_file = descendant_of_narc->end_offset;

    //First reinsert the file inside the NARC Archive
    const auto file_new_contents_compressed = Get_Compressed_File(file_new_contents, file_metadata->compression_type);
    const int diff { Reinsert_Child_File(narc_file, descendant_of_narc, file_new_contents_compressed, narc_file->archive_contents) };

    if (diff != 0)
    {
        //Then update its File Allocation Table if other files in the FS have been moved (occurs when
        //the reinserted file has a different size from before)
        QByteArray copy_archive_for_narc_header { narc_file->archive_contents };
        MemoryStream narc_contents_stream { std::move(copy_archive_for_narc_header) };
        const NARCHeader header { narc_contents_stream };

        const QByteArray old_fat { narc_file->archive_contents.mid(header.fat_offset, header.number_of_files * 8) };
        //We don't pass the physical address of the file, but its equivalent for the FAT.
        const QByteArray new_fat { Build_New_File_Allocation_Table(old_fat, old_end_offset_file - header.fimg_offset - 8, diff) };
        Q_ASSERT(old_fat.size() == new_fat.size());

        //And rewrite the FAT in the NARC
        narc_file->archive_contents.replace(header.fat_offset, old_fat.size(), new_fat);

        //Lastly, update the end offset of the reinserted file in the FAT.
        const quint32 address_end_offset_file_in_fat { header.fat_offset + descendant_of_narc->id * 8 + 4 };
        //Don't forget that for NARCs, the offsets stored in the FAT are not the absolute addresses! (Hence the subtractions)
        const quint32 physical_end_offset { descendant_of_narc->end_offset - header.fimg_offset - 8 };
        narc_file->archive_contents[address_end_offset_file_in_fat] = physical_end_offset & 0xFF;
        narc_file->archive_contents[address_end_offset_file_in_fat+1] = (physical_end_offset >> 8) & 0xFF;
        narc_file->archive_contents[address_end_offset_file_in_fat+2] = (physical_end_offset >> 16) & 0xFF;
        narc_file->archive_contents[address_end_offset_file_in_fat+3] = (physical_end_offset >> 24) & 0xFF;

        //Also update the size of the NARC Archive.
        const quint32 new_fimg_size { header.fimg_size + diff };
        narc_file->archive_contents[header.fimg_offset+4] = (new_fimg_size) & 0xFF;
        narc_file->archive_contents[header.fimg_offset+5] = (new_fimg_size >> 8) & 0xFF;
        narc_file->archive_contents[header.fimg_offset+6] = (new_fimg_size >> 16) & 0xFF;
        narc_file->archive_contents[header.fimg_offset+7] = (new_fimg_size >> 24) & 0xFF;
    }
}

void Reinsert_Child_File_and_Rebuild_NitroFS(QFile &nds_rom, NitroFile *child, const QByteArray &file_new_contents, NitroFileMetadata const* const file_metadata)
{
    Q_ASSERT(child->Get_Ancestor_NARC() == nullptr);
    NitroDirectory* root_directory { child->parent };
    while (root_directory->parent != nullptr)
    {
        root_directory = root_directory->parent;
    }

    const auto old_end_offset_file = child->end_offset;

    const auto file_new_contents_compressed = Get_Compressed_File(file_new_contents, file_metadata->compression_type);
    nds_rom.seek(0);
    QByteArray nds_rom_contents { nds_rom.readAll() };
    const int diff { Reinsert_Child_File(root_directory, child, file_new_contents_compressed, nds_rom_contents) };

    if (diff != 0)
    {
        //Now we recreate the FAT for the ROM if the size of the file inserted is different from before
        //(thus causing other file in the ROM to be shifted).
        QByteArray buffer_for_nds_file_stream { nds_rom_contents };
        const MemoryStream nds_file_stream { std::move(buffer_for_nds_file_stream) };
        const NDSHeader header { nds_file_stream };

        const QByteArray old_fat { nds_rom_contents.mid(header.fat_offset, header.fat_size) };
        const QByteArray new_fat { Build_New_File_Allocation_Table(old_fat, old_end_offset_file, diff) };
        Q_ASSERT(old_fat.size() == new_fat.size());
        nds_rom_contents.replace(header.fat_offset, header.fat_size, new_fat);

        //Lastly, update the end offset of the reinserted file in the FAT.
        const quint32 address_end_offset_file_in_fat { header.fat_offset + child->id * 8 + 4 };
        nds_rom_contents[address_end_offset_file_in_fat] = child->end_offset & 0xFF;
        nds_rom_contents[address_end_offset_file_in_fat+1] = (child->end_offset >> 8) & 0xFF;
        nds_rom_contents[address_end_offset_file_in_fat+2] = (child->end_offset >> 16) & 0xFF;
        nds_rom_contents[address_end_offset_file_in_fat+3] = (child->end_offset >> 24) & 0xFF;
    }

    //Rewrite the new ROM, erasing its old contents.
    nds_rom.resize(0);
    const qint64 nb_bytes { nds_rom.write(nds_rom_contents) };
    if (nb_bytes != nds_rom_contents.size())
        qDebug() << "Error when writing NDS file after reinserting file, wrote " << nb_bytes << " bytes instead of " << nds_rom_contents.size();
    else
        nds_rom.flush();
}

QByteArray Get_Compressed_File(const QByteArray &file_contents, CompressionType type)
{
    switch (type)
    {
    case CompressionType::Raw:
    {
        return file_contents;
    }
        break;
    case CompressionType::LZ77:
    {
        const std::vector<char> contents { lz77::compress(file_contents.begin(), file_contents.end()) };
        return QByteArray{contents.data(), static_cast<int>(contents.size())};
    }
        break;
    default:
    {
        return file_contents;
    }
        break;
    }
}
