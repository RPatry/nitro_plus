#ifndef NITROFS_HPP
#define NITROFS_HPP

#include <QString>
#include <QIcon>
#include <QFile>
#include <QFileDevice>
#include <QDebug>
#include <memory>
#include <vector>
#include <utility>
#include "memorystream.hpp"

struct NDSHeader {
    explicit NDSHeader(MemoryStream const& nds_rom);

    const QString game_title {};
    const QString game_code {};
    const QString maker_code {};
    const quint8 rom_version {};
    const quint32 arm9_rom_offset {}, arm9_entry_address {}, arm9_ram_address {};
    const quint32 arm7_rom_offset {}, arm7_entry_address {}, arm7_ram_address {};
    const quint32 fnt_offset {}, fnt_size {};
    const quint32 fat_offset {}, fat_size {};
    const quint32 icon_title_offset {};

};

struct NARCHeader {
    explicit NARCHeader(MemoryStream const& narc_archive);

    const quint32 number_of_files {};
    const quint32 fat_offset {}, fat_size {};
    const quint32 fnt_offset {}, fnt_size {};
    const quint32 fimg_offset {}, fimg_size {};
};

struct NitroDirectory;
struct NitroNARC;

struct NitroNode {
public:
    NitroNode(const quint16 _id, QString const& _name, NitroDirectory* const _parent) :
        id{_id}, name{_name}, parent{_parent}
    {
    }
    NitroNode() = delete;
    NitroNode(NitroNode const&) = delete;
    NitroNode& operator=(NitroNode const&) = delete;
    NitroNode(NitroNode &&) = default;
    NitroNode& operator=(NitroNode &&) = default;
    virtual ~NitroNode();

    const quint16 id {};
    const QString name {};
    NitroDirectory* const parent {};

    QString Get_Node_Path(void) const noexcept;

    /*!
     * \brief Get_Ancestor_NARC Find the closest NARC Archive in the ancestor hierarchy.
     * \return The NARC node if one exists in the hierarchy; nullptr otherwise.
     */
    NitroNARC* Get_Ancestor_NARC(void) const noexcept;
};

struct NitroFile;

struct NitroDirectory : public virtual NitroNode {
public:
    NitroDirectory(const quint16 _id, QString const& _name, NitroDirectory* const _parent) :
        NitroNode(_id, _name, _parent)
    {  }
    NitroDirectory() = delete;
    NitroDirectory(NitroDirectory const&) = delete;
    NitroDirectory& operator=(NitroDirectory const&) = delete;
    NitroDirectory(NitroDirectory &&) = default;
    NitroDirectory& operator=(NitroDirectory&&) = default;
    virtual ~NitroDirectory();

    /*! When reinserting a file in a Nitro File System (NDS ROM or NARC Archive), if the file
     * ends up moving some other files in the FS (because of a different size), then this method needs to be
     * called. The files to be moved must belong to the exact same file system (don't do anything inside child NARC archives) */
    void Move_Children_Files_Offsets(quint32 old_end_offset_updated_file, qint32 diff_offset);

    /*! Stores all children directories, but actual Nitro directories (NARC archives are stored as files). */
    std::vector<std::unique_ptr<NitroDirectory>> children_directories {};
    /*! Stores all the children files, including NARC Archives. */
    std::vector<std::unique_ptr<NitroFile>> children_files {};

    size_t ChildrenNumber(void) const noexcept {
        return children_directories.size() + children_files.size();
    }

    /*!
     * \brief Count the number of files and subdirectories in this directory, recursively.
     * \return A pair of sizes, first is the number of subdirectories, second the number of files
     */
    std::pair<size_t, size_t> Count_Directories_and_Files(void) const noexcept;

    /*! Find the biggest ID of all the files stored in this directory and its subdirectories. */
    quint16 Get_Max_File_ID(void) const noexcept;
};

enum class CompressionType;

struct Failed_Extraction_Exception
{

};

struct NitroFileMetadata;

struct NitroFile : public virtual NitroNode {
public:
    NitroFile(const quint16 _id, QString const& _name, NitroDirectory* const _parent,
                   const quint32 _start, const quint32 _end) :
        NitroNode(_id, _name, _parent), start_offset{_start}, end_offset{_end}
    {  }
    NitroFile() = delete;
    NitroFile(NitroFile const&) = delete;
    NitroFile& operator=(NitroFile const&) = delete;
    NitroFile(NitroFile &&) = default;
    NitroFile& operator=(NitroFile&&) = default;
    virtual ~NitroFile();

    //Useful to know where the bytes of this file are located in the parent NDS ROM or NARC Archive (uncompressed).
    quint32 start_offset {}, end_offset {};

    /*!
     * \brief Extract this file from the NDS Rom
     * \param nds_file The contents of the ROM. Note: if the file is a child of a NARC archive inside a NDS ROM, then this parameter will be ignored.
     * \param type The assumed compression type of the file.
     * \return  The bytes of this file.
     * \throw Failed_Extraction_Exception If for some reason the file offsets are invalid, or if the decompression failed (because of an invalid type of compression).
     */
    QByteArray Extract_File(QIODevice &nds_file, CompressionType type) const;

    QByteArray Extract_File(MemoryStream & stream_nds_file, CompressionType type) const;

    /*!
     * \brief Extract_Raw_File Extract the raw bytes of this file, regardless of its compression type
     * \param nds_file The current NDS ROM (note: if this file is a child of a NARC Archive, then nds_file will be ignored)
     * \return
     */
    QByteArray Extract_Raw_File(QIODevice& nds_file) const;

    /*! Reinsert this file into its file system replacing it with new bytes
     */
    void Reinsert_File(QFile &nds_file, QByteArray const& new_file_contents, NitroFileMetadata const* file_metadata);
};

/*!
 * Represents a NARC archive embedded in a Nitro file system (that is, and it's important to note,
 * a NARC archive opened on its own will use NitroDirectory like NDS ROMs).
 * Conceptually, it's both a directory and a file, which can be tricky in some code.
 * The file part means we can extract and reinsert the archive, with possibly some compression type.
 * The directory part means we can display
 * its children files in the tree hierarchy, as though they belonged to the NDS ROM's Nitro file system.
 */
struct NitroNARC : NitroDirectory, NitroFile
{
    /* I usually prefer uniform initialization, yet I make an exception here and use parentheses instead.
     * The reason is that I am stuck with g++5 for now, and it appears to be bugged for virtual inheritance.
     * See: http://stackoverflow.com/questions/38466871/invoking-constructors-during-virtual-inheritance-with-c/38467534
     */
    NitroNARC(const quint16 _id, QString const& _name, NitroDirectory* const _parent, const quint32 _start=0, const quint32 _end=0) :
        NitroNode(_id, _name, _parent),
        NitroDirectory(_id, _name, _parent),
        NitroFile(_id, _name, _parent, _start, _end)
    {
    }
    NitroNARC() = delete;

    NitroNARC(NitroNARC const&) = delete;
    NitroNARC& operator=(NitroNARC const&) = delete;
    NitroNARC(NitroNARC&&) = default;
    NitroNARC& operator=(NitroNARC&&) = default;
    virtual ~NitroNARC();

    /*! \brief All the bytes contained in this archive; uncompressed!
      */
    QByteArray archive_contents {};

};

std::unique_ptr<NitroDirectory> Parse_ROM_for_NitroFS(MemoryStream const& nds_rom);

struct NARC_Parsing_Failure
{
};

/*!
 * \brief Parse_NARC_for_NitroFS
 * \param narc_file
 * \return
 * \throw NARC_Parsing_Failure if shit hits the fan
 */
std::unique_ptr<NitroNARC> Parse_NARC_for_NitroFS(MemoryStream const& narc_file, NitroDirectory *parent, quint16 id, const QString name, quint32 start_offset, quint32 end_offset);

/*!
 * \return The icon for the NDS ROM; if no icon is in the ROM, an icon of size 0x0 will be returned.
 */
QIcon Get_NDS_ROM_Icon(MemoryStream const& nds_rom);

struct NitroFileMetadata;
/*!
 * \brief Check_for_NARC_with_Current_Compression_Type Check if with the current compression type, the file is actually a NARC Archive.
 * Warning: if it is, it will be replaced in its parent directory by a NitroNARC node! The metadata will undergo a similar treatment.
 * \param file
 * \param file_metadata
 */
void Check_for_NARC_with_Current_Compression_Type(MemoryStream &stream_nds_file, NitroFile const* file, NitroFileMetadata const* file_metadata);

QByteArray Get_Compressed_File(QByteArray const& file_contents, CompressionType type);

void Reinsert_Child_File_and_Rebuild_NARC(NitroNARC* narc_file, NitroFile* descendant_of_narc, QByteArray const& file_new_contents, const NitroFileMetadata *file_metadata);

void Reinsert_Child_File_and_Rebuild_NitroFS(QFile &nds_rom, NitroFile* child, QByteArray const& file_new_contents, const NitroFileMetadata * const file_metadata);

#endif // NITROFS_HPP
