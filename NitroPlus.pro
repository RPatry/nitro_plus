#-------------------------------------------------
#
# Project created by QtCreator 2017-02-13T00:10:46
#
#-------------------------------------------------

QT       += core gui

QMAKE_CXXFLAGS = -Wall -Wextra -Werror -Wpedantic -save-temps=obj
CONFIG += c++17

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NitroPlus
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        main_window.cpp \
    memorystream.cpp \
    nitrofs.cpp \
    treemodel.cpp \
    aboutnitrodialog.cpp \
    lz77.tpp \
    metadata.cpp \
    filedatadialog.cpp \
    qhexedit2/src/qhexedit.cpp \
    qhexedit2/src/chunks.cpp \
    qhexedit2/src/commands.cpp \
    algos.tpp \
    hexeditordialog.cpp

HEADERS  += main_window.hpp \
    memorystream.hpp \
    nitrofs.hpp \
    treemodel.hpp \
    aboutnitrodialog.hpp \
    lz77.hpp \
    metadata.hpp \
    filedatadialog.hpp \
    qhexedit2/src/qhexedit.h \
    qhexedit2/src/chunks.h \
    qhexedit2/src/commands.h \
    algos.hpp \
    hexeditordialog.hpp

FORMS    += main_window.ui \
    aboutnitrodialog.ui \
    filedatadialog.ui \
    hexeditordialog.ui

RESOURCES += \
    resources.qrc
