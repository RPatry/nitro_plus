#include "aboutnitrodialog.hpp"
#include "ui_aboutnitrodialog.h"

AboutNitroDialog::AboutNitroDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutNitroDialog)
{
    ui->setupUi(this);
    //Get rid of "?" button
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setMaximumSize(size());
    setMinimumSize(size());
}

AboutNitroDialog::~AboutNitroDialog()
{
    delete ui;
}
