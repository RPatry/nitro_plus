#include "main_window.hpp"
#include "ui_main_window.h"
#include <algorithm>
#include <vector>
#include <QFileDialog>
#include "nitrofs.hpp"
#include "memorystream.hpp"
#include "aboutnitrodialog.hpp"
#include "filedatadialog.hpp"
#include "hexeditordialog.hpp"
#include <QFile>
#include <QFileInfo>
#include <QErrorMessage>
#include <QDebug>
#include <QMessageBox>
#include <QByteArray>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Setup_Connections();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OpenROMDialogue()
{
    auto filename = QFileDialog::getOpenFileName(this, tr("Open a NDS ROM..."), "", "Nintendo DS ROM Files (*.nds);;All files(*.*)");
    if (filename.size() == 0)   //An empty filename; no need to return an error, the user just cancelled opening
        return;

    QFileInfo nds_file_info { filename };
    if (!nds_file_info.isReadable())
    {
        Show_Error_Dialog(tr("The file you opened does not appear to be readable."));
        return;
    }
    //Open the Nintendo DS ROM and parse its filesystem
    QFile nds_file { filename };
    nds_file.open(QFile::ReadOnly);
    QByteArray bytes_nds_rom { nds_file.readAll() };
    MemoryStream file_stream { std::move(bytes_nds_rom) };
    unique_ptr<NitroDirectory> root_directory {};
    if (file_stream.Read_String(0, 4) == "NARC")
    {
        //root_directory = Parse_NARC_for_NitroFS(file_stream);
        qDebug() << "Tried to open a NARC Archive as is - that's still unsupported";
    }
    else
    {
        root_directory = Parse_ROM_for_NitroFS(file_stream);
    }
    const QIcon new_icon_for_application { Get_NDS_ROM_Icon( file_stream) };
    if (!new_icon_for_application.isNull())
    {
        this->setWindowIcon(new_icon_for_application);
    }

    //Metadata
    std::unique_ptr<NitroDirectoryMetadata> metadata_root {};
    const NDSHeader header { file_stream };
    metadata_root = Get_Metadata_for_ROM(header.game_title, filename);
    if (metadata_root.get() == nullptr)
    {
        metadata_root = Parse_File_System_Guess_Metadata<NitroDirectoryMetadata>(file_stream, root_directory.get());
    }

    //Setup the new tree model
    QItemSelectionModel* old_selection_model { ui->treeView->selectionModel() };
    this->tree_model = make_unique<TreeModel>(std::move(root_directory), std::move(metadata_root));
    this->ui->treeView->setModel(this->tree_model.get());
    if (old_selection_model != nullptr)
        disconnect(old_selection_model, &QItemSelectionModel::selectionChanged, this, &MainWindow::Tree_View_Selection_Changed);
    delete old_selection_model;
    QItemSelectionModel* new_selection_model { ui->treeView->selectionModel() };
    connect(new_selection_model, &QItemSelectionModel::selectionChanged, this, &MainWindow::Tree_View_Selection_Changed);

    this->current_nds_file = filename;
    this->setWindowTitle(QString{"NitroPlus - %1"}.arg(this->current_nds_file));
}

void MainWindow::Setup_Connections()
{
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::OpenROMDialogue);
    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionExtract_selection, &QAction::triggered, this, &MainWindow::Extract_Selection);
    connect(ui->actionReinsert_file, &QAction::triggered, this, &MainWindow::Reinsert_File);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::Show_About_Dialog);
    connect(ui->actionFile_info, &QAction::triggered, this, &MainWindow::Show_Info_Dialog);
    connect(ui->actionHex_viewer, &QAction::triggered, this, &MainWindow::Show_Hex_Editor);
}

void MainWindow::Show_Error_Dialog(const QString & error_message)
{
    QErrorMessage message_dialog{this};
    message_dialog.showMessage(error_message);
}

void MainWindow::Tree_View_Selection_Changed(const QItemSelection &new_items, const QItemSelection &old_items)
{
    Q_UNUSED(old_items);
    Q_UNUSED(new_items);
    const auto selected_indexes = ui->treeView->selectionModel()->selectedIndexes();
    const QString new_status_string { this->tree_model->Tree_View_Selection_Changed(selected_indexes) };
    this->ui->statusBar->showMessage(new_status_string);
    this->ui->actionExtract_selection->setEnabled(selected_indexes.size() > 0);
    this->ui->actionFile_info->setEnabled(this->tree_model->Single_File_is_Selected(selected_indexes));
    this->ui->actionReinsert_file->setEnabled(this->ui->actionFile_info->isEnabled());
    this->ui->actionHex_viewer->setEnabled(this->ui->actionFile_info->isEnabled());
}

void MainWindow::Extract_Selection()
{
    const QString directory_path { QFileDialog::getExistingDirectory(this, tr("Select a destination directory...")) };
    const QFileInfo file_info { directory_path };
    if (!file_info.exists())
    {
        //I'm not sure this case is even possible with the directory dialog, but better safe than sorry.
        Show_Error_Dialog(tr("This directory does not exist!"));
        return;
    }
    if (!file_info.isWritable())
    {
        Show_Error_Dialog(tr("This directory is not writable!"));
        return;
    }
    //Reopen the NDS file if it still exists.
    QFile nds_file { this->current_nds_file };
    nds_file.open(QFile::ReadOnly);
    set<NitroFile*> failed_files_extractions { this->tree_model->Extract_Indexes(directory_path, nds_file,
                                                              ui->treeView->selectionModel()->selectedIndexes()) };
    this->Negotiate_for_Failed_Extractions(failed_files_extractions.begin(), failed_files_extractions.end());

    this->statusBar()->showMessage(tr("Done extracting!"));
}

void MainWindow::Reinsert_File()
{
    //Very long function. Can't do much better aside from splitting into several others for little benefit.

    //First try to get the path to the new file from the user.
    auto filename = QFileDialog::getOpenFileName(this, tr("Select a file to insert"), "", "All files(*.*)");
    if (filename.size() == 0)   //An empty filename; no need to return an error, the user just cancelled opening
        return;

    //A few assertion on the file provided by the user: readable? Not empty?
    QFileInfo file_info { filename };
    if (!file_info.isReadable())
    {
        Show_Error_Dialog(tr("The file you opened does not appear to be readable."));
        return;
    }
    if (file_info.size() == 0)
    {
        Show_Error_Dialog(tr("You tried to insert an empty file."));
        return;
    }

    QFile file_to_reinsert{ filename };
    file_to_reinsert.open(QFile::ReadOnly);
    const QByteArray bytes_file_to_reinsert { file_to_reinsert.readAll() };

    //Try to reopen the DS ROM file, also checking if opening succeeds.
    QFile file_nds_rom { this->current_nds_file };
    if (!file_nds_rom.open(QFile::ReadWrite))
    {
        Show_Error_Dialog(QString{tr("Failed to open the Nintendo DS ROM file for reading and writing at %1")}.arg(this->current_nds_file));
        return;
    }

    //Then we can start worrying about the file to reinsert itself.
    const auto selected_indexes = this->ui->treeView->selectionModel()->selectedIndexes();
    if (!this->tree_model->Single_File_is_Selected(selected_indexes))
    {
        Show_Error_Dialog(tr("Tried to reinsert a file, but selection is not a unique file"));
        return;
    }
    const auto index = selected_indexes[0];
    NitroFile* const file { this->tree_model->Get_Selected_File(index) };

    //Fetch the file metadata.
    const NitroFileMetadata* const file_metadata { dynamic_cast<const NitroFileMetadata*>(this->tree_model->Find_Equivalent_Metadata_Node(file)) };
    if (file_metadata == nullptr)
    {
        qDebug() << "Could not find the metadata for the file to reinsert";
        return;
    }
    //Now for the heart of the work
    file->Reinsert_File(file_nds_rom, bytes_file_to_reinsert, file_metadata);

    file_nds_rom.seek(0);
    MemoryStream stream { file_nds_rom.readAll() };
    //And lastly, because we're thorough, we check if the file inserted is a NARC File, creating a subfile system if necessary.
    this->tree_model->Start_File_Type_Replacing(index);
    Check_for_NARC_with_Current_Compression_Type(stream, file, file_metadata);
    this->tree_model->End_File_Type_Replacing();
    this->ui->statusBar->showMessage(tr("Done reinserting!"));
}

void MainWindow::Show_About_Dialog()
{
    AboutNitroDialog dialog {};
    dialog.exec();
}

void MainWindow::Show_Info_Dialog()
{
    const auto selected_indexes = this->ui->treeView->selectionModel()->selectedIndexes();
    if (!this->tree_model->Single_File_is_Selected(selected_indexes))
        return;
    const auto index = selected_indexes[0];
    const NitroFile* const file { this->tree_model->Get_Selected_File(index) };
    NitroNodeMetadata* const node_metadata { this->tree_model->Find_Equivalent_Metadata_Node(file) };
    NitroFileMetadata* const file_metadata { dynamic_cast<NitroFileMetadata*>(node_metadata) };
    if (file_metadata != nullptr)
    {
        QFile nds_file { this->current_nds_file };
        nds_file.open(QFile::ReadOnly);
        auto bytes = file->Extract_Raw_File(nds_file);
        FileDataDialog dialog{this, file, file_metadata->compression_type, std::move(bytes)};
        const auto result = dialog.exec();
        if (result == FileDataDialog::Accepted)
        {
            if (file_metadata->compression_type != dialog.Get_Compression_Type())
            {
                nds_file.seek(0);
                QByteArray all_bytes { nds_file.readAll() };
                MemoryStream stream{ std::move(all_bytes) };
                file_metadata->compression_type = dialog.Get_Compression_Type();
                this->tree_model->Start_File_Type_Replacing(index);
                Check_for_NARC_with_Current_Compression_Type(stream, file, file_metadata);
                this->tree_model->End_File_Type_Replacing();
            }
        }
    }
    else
    {
        qDebug() << "Information about a file whose node is not a file!";
    }
}

void MainWindow::Show_Hex_Editor()
{
    const auto selected_indexes = this->ui->treeView->selectionModel()->selectedIndexes();
    if (!this->tree_model->Single_File_is_Selected(selected_indexes))
        return;
    const auto index = selected_indexes[0];
    NitroFile* const file { this->tree_model->Get_Selected_File(index) };
    NitroNodeMetadata* const node_metadata { this->tree_model->Find_Equivalent_Metadata_Node(file) };
    NitroFileMetadata* const file_metadata { dynamic_cast<NitroFileMetadata*>(node_metadata) };
    try {
    if (file_metadata != nullptr)
    {
        QFile nds_file { this->current_nds_file };
        nds_file.open(QFile::ReadOnly);
        auto bytes = file->Extract_File(nds_file, file_metadata->compression_type);
        nds_file.close();
        HexEditorDialog dialog{std::move(bytes), file->Get_Node_Path(), this};
        dialog.exec();
    }
    }
    catch (Failed_Extraction_Exception)
    {
        std::vector<NitroFile*> failed_vec = {file};
        Negotiate_for_Failed_Extractions(failed_vec.begin(), failed_vec.end());
    }
}

template<typename NitroFileIter>
void MainWindow::Negotiate_for_Failed_Extractions(const NitroFileIter begin, const NitroFileIter end)
{
    auto iter_current_file = begin;
    auto nb_files = std::distance(begin, end) + 1;
    bool yes_all { false };
    while (iter_current_file != end)
    {
        NitroFile* const file { *iter_current_file };
        nb_files--;
        iter_current_file++;

        if (yes_all)   //The user already clicked on "Yes to all" button some time ago; so directly change this file's compression type.
        {
            this->tree_model->Change_Compression_Type_to_Raw(file);
            continue;
        }

        const QString mess_box_text { QString{tr("%1 could not be extracted. Its compression format is"
                                                 " likely incorrect. Would you like to change it to \"Raw\"?")
                                             }.arg(file->Get_Node_Path()) };
        const QString title { QString{ tr("Failed extraction - %1 more files remaining") }.arg(nb_files) };
        QMessageBox::StandardButtons buttons { QMessageBox::Yes | QMessageBox::YesToAll | QMessageBox::No | QMessageBox::NoToAll };
        //Display the dialog and extract the clicked button
        const QMessageBox::StandardButton button {
            QMessageBox::question(this, title, mess_box_text, buttons, QMessageBox::Yes) };
        switch (button)
        {
            case QMessageBox::YesAll:
                yes_all = true;
                [[fallthrough]];
            case QMessageBox::Yes:
                this->tree_model->Change_Compression_Type_to_Raw(file);
                break;
            case QMessageBox::No:
                continue;
            case QMessageBox::NoToAll:
                return;
            default:
                qDebug() << "Impossible button was pushed";
                continue;
        }

    }
}
