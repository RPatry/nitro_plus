#ifndef LZ77_HPP
#define LZ77_HPP

#include <vector>

namespace lz77 {

    template<class InputIterator>
    std::vector<char> decompress(InputIterator begin, InputIterator end);

    template<class InputIterator>
    std::vector<char> compress(InputIterator begin, InputIterator end);

}

#include "lz77.tpp"

#endif
