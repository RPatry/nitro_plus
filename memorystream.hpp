#ifndef MEMORYSTREAM_HPP
#define MEMORYSTREAM_HPP

#include <QByteArray>
#include <QString>

/*!
 * \brief A wrapper of a vector of bytes, from which we can extract integers of various size (always
 * little-endian though because it's from the Nintendo DS).
 */
class MemoryStream
{
public:
    explicit MemoryStream(QByteArray&& bytes_stream);

    /*!
     * \brief Read an unsigned little-endian 32-bit integer from the stream
     * \param the position of the least significant byte, needs to point to a valid byte inside the stream! This will not be checked.
     * \return The read integer.
     * \throw If one of the bytes is outside the reach of the stream
     */

    quint32 Read_U32(int pos) const;
    /*!
     * \brief Read an unsigned little-endian 16-bit integer from the stream
     * \param the position of the least significant byte, needs to point to a valid byte inside the stream! This will not be checked.
     * \return The read integer.
     * \throw If one of the bytes is outside the reach of the stream
     */
    quint16 Read_U16(int pos) const;

    /*!
     * \brief Read a byte from the stream
     * \param the position of the byte in the file, needs to point to a valid byte inside the stream! This will not be checked.
     * \return The read byte
     * \throw If the position is outside the boundaries of the stream
     */
    quint8  Read_U8 (int pos) const {
        return static_cast<quint8>(bytes_stream.at(pos));
    }
    /*!
     * \brief Read an ASCII string from the stream, not null-terminated!
     * \param the position of the string
     * \param the length of the string in bytes
     * \return the string, which may be empty if pos + length exceeds the length of the file
     */
    QString Read_String(int pos, int length) const noexcept;

    int Size(void) const noexcept {
        return bytes_stream.size();
    }

    QByteArray const& Get_All_Bytes() const noexcept {
        return bytes_stream;
    }

    QByteArray& Get_All_Bytes() noexcept {
        return bytes_stream;
    }

private:
    QByteArray bytes_stream {};
};

#endif // MEMORYSTREAM_HPP
