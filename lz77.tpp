#include <vector>
#include <deque>

namespace lz77 {


template<class InputIterator>
unsigned int read_size(InputIterator iterator_size)
{
    const unsigned char byte0 { static_cast<unsigned char>(*iterator_size) };
    iterator_size++;
    const unsigned char byte1 { static_cast<unsigned char>(*iterator_size) };
    iterator_size++;
    const unsigned char byte2 { static_cast<unsigned char>(*iterator_size) };
    const unsigned int size { static_cast<unsigned int>(byte0 | (byte1 << 8) | (byte2 << 16)) };
    return size;
}

template<class InputIterator>
std::vector< char> decompress(InputIterator begin, InputIterator end) {
    /* For reference: https://www.akkit.org/info/gbatek.htm#biosdecompressionfunctions (SWI 11 and 12) */
    const unsigned char header = *begin;
    InputIterator iter { begin + 1 };
    std::vector< char> result {};
    if (header == 0x10) {
        const unsigned int size { read_size(iter) };
        result.reserve(size);
        iter += 3;

        while (iter != end && result.size() < size) {
            const  char flag_byte { *iter };
            iter++;   //Point to the next Block
            //Process the next 8 Blocks
            for (int i = 7; i >= 0 && result.size() < size; i--) {
                const bool compressed_block { ((flag_byte >> i) & 1) > 0 };
                if (!compressed_block) {
                    //Just copy one byte from source to destination.
                    result.push_back(*iter);
                    iter++;   //Point to the next Block or possibly a flag byte
                }
                else {
                    //Copy a number of bytes from earlier in the destination buffer.
                    const unsigned char byte_0_block_1 { static_cast<unsigned char>(*iter) };
                    const unsigned int nb_bytes_copy { 3 + (static_cast<unsigned int>(byte_0_block_1 >> 4) & 0xF) };
                    const unsigned int distance_msb { static_cast<unsigned int>(byte_0_block_1 & 0xF) };
                    iter++;     //Point to the distance LSBs.
                    const unsigned char byte_1_block_1 { static_cast<unsigned char>(*iter) };
                    const unsigned int distance = static_cast<unsigned int>(distance_msb << 8) | byte_1_block_1;
                    //No standard algorithm to solve this problem. Neither copy nor copy_backwards work because
                    //of undefined behaviour if the two ranges overlap. Our problem is that the end of the source range *might* not exist yet
                    //and may only be fully created over time with copying. So we resort to copying by hand.
                    for (unsigned int nb = 0; nb < nb_bytes_copy && result.size() < size; nb++) {
                        const size_t size_result {  result.size() };
                        const size_t pos_byte_to_copy { size_result - distance - 1 };
                        result.push_back(result.at(pos_byte_to_copy));   //Could be optimized perhaps?
                    }
                    iter++;   //Point to the next Block or possibly a flag byte
                } } } }
    return result;
}

/*! An abstraction over the sliding window concept. As the program progresses through the source buffer during LZ77
 * compression, this struct must be informed of the bytes that are encountered, so it can record them.
 * As new bytes are added, old bytes will be forgotten so that the sliding window never exceeds its maximum size
 * You can ask it all the locations a specific byte can be found in the sliding window.
*/
template<size_t WindowLength>
struct SlidingWindow
{
    SlidingWindow() :
        bytes_locations_lookup{256}
    {   }
    ~SlidingWindow()
    {   }

    void Add_Byte(const char new_byte)
    {
        if (this->bytes_sliding_window.size() > 0)
        {
            /* The whole sliding window will be permanently "one-off", so to speak.
             * The reason for that is that LZ77 compression needs to also work for 16 bits units when expanding, on NDS/GBA hardware,
             * With 16-bit units, Block 1 types need to start copying before the last byte in the output buffer (go see GBATEK! The caution part: Disp must be > 0)
             * This is why, when we register a new byte, only then is the previous byte noticed as a potential copy target.
             * Because it's now safe for the previous byte, there is more than one byte separating it from the destination.
             * In practice, what this means is that files might not be compressed optimally, but they should
             * be close enough, AND they will work with 16-bits units for expanding. That's the most important part
             */
            this->bytes_locations_lookup[static_cast<unsigned char>(this->bytes_sliding_window.back())]
                                    .push_back(this->offset_last_byte - 1);
        }
        this->bytes_sliding_window.push_back(new_byte);
        if (this->bytes_sliding_window.size() > WindowLength)
        {
            this->Pop_Byte();
        }
        this->offset_last_byte++;
    }

    std::deque<size_t> const& Get_Byte_Positions(const char byte) const noexcept
    {
        return this->bytes_locations_lookup[static_cast<unsigned char>(byte)];
    }

    static const size_t Max_Window_Size = WindowLength;

private:
    /*! This 256-elements long vector is indexed by bytes values: it will return a list of locations
     * where you can find the byte in the sliding window. The locations are offsets from the beginning
     * of the source buffer. */
    std::vector<std::deque<size_t>> bytes_locations_lookup {};
    std::deque<char> bytes_sliding_window {};
    size_t offset_last_byte{};

    void Pop_Byte()
    {
        if (bytes_sliding_window.size() > 0)
        {
            const unsigned char byte_val { static_cast<unsigned char>(bytes_sliding_window.front()) };
            std::deque<size_t>& bytes_locations = bytes_locations_lookup[byte_val];
            if (bytes_locations.size() > 0)
            {
                bytes_locations.pop_front();
            }
            bytes_sliding_window.pop_front();
        }
    }
};

using SlidingWindowGBA = SlidingWindow<4096>;

//Describes a range a bytes found earlier in the stream to copy. Useful for making Block 1 types when compressing.
struct PositionToCopy
{
    PositionToCopy(int _distance_from_current, int _nb_bytes_to_copy) :
        distance_from_current{_distance_from_current}, nb_bytes_to_copy{_nb_bytes_to_copy}
    {   }
    const int distance_from_current {};
    const int nb_bytes_to_copy {};
};

/* If we consider the current byte to compress in the stream (and the bytes following it), this function
 * will try to locate the same bytes earlier on in the so-called sliding window.
* If the same byte as the current wasn't found in the sliding window,
* then the number of bytes to copy will be -1, and the distance from current is to be ignored */
template<class InputIterator>
PositionToCopy find_position_to_copy(InputIterator begin_input_range, InputIterator current_position, InputIterator end_input_range,
                                     SlidingWindowGBA const& sliding_window)
{
    const auto& offsets_for_byte = sliding_window.Get_Byte_Positions(*current_position);
    int max_nb_bytes_common { -1 };
    int offset_biggest_range { -1 };
    for (const size_t offset : offsets_for_byte)
    {
        int nb_bytes_common { 1 };
        InputIterator iter_sliding_window { begin_input_range + offset + 1 }, iter_from_current_byte { current_position + 1 };
        bool bytes_equal { *iter_sliding_window == *iter_from_current_byte };
        /* The range can be no longer than 18 bytes (15 + 3), as compressed in Block 1 types.
         * But we stop as soon as we find the ranges are different. Or if we reached the end of the input array, of course. */
        while (bytes_equal && nb_bytes_common < 18 && iter_from_current_byte != end_input_range)
        {
            bytes_equal =  *iter_sliding_window == *iter_from_current_byte;
            iter_sliding_window++;
            iter_from_current_byte++;
            nb_bytes_common += static_cast<int>(bytes_equal); //Will only be incremented if the bytes are still equal.
        }
        if (nb_bytes_common > max_nb_bytes_common)
        {
            max_nb_bytes_common = nb_bytes_common;
            offset_biggest_range = offset;
            /* No need to go on any further, we found a perfectly suitable candidate already.
             * Might not look like much, but this test reduced one of my perf benchmarks by almost 50% */
            if (nb_bytes_common == 18)
            {
                break; //No need to look any further, we aren't gonna do better, that's already the maximum size to copy.
            }
        }
    }
    const int offset_current_byte { static_cast<int>(std::distance(begin_input_range, current_position)) };
    const PositionToCopy result {offset_current_byte - offset_biggest_range - 1, max_nb_bytes_common};
    return result;
}

//Return a vector for compressed data with the correct header inserted.
template<class InputIterator>
std::vector<char> make_default_output_array(InputIterator const& begin, InputIterator const& end)
{
    const auto size_decomp_data = std::distance(begin, end);
    const char byte1 { static_cast<char>(size_decomp_data & 0xFF) };
    const char byte2 { static_cast<char>((size_decomp_data >> 8) & 0xFF) };
    const char byte3 { static_cast<char>((size_decomp_data >> 16) & 0xFF) };
    std::vector<char> result { 0x10, byte1, byte2, byte3 };
    return result;
}

template<class InputIterator>
std::vector<char> compress(InputIterator begin, InputIterator end)
{
    std::vector<char> out { make_default_output_array(begin, end) };
    out.push_back(0);   //We insert a dummy byte for the first byte.
    size_t position_latest_flag_byte { 4 };    //Keeps track of where we'll insert the Flag Byte once we've written 8 blocks.
    char flag_byte {};    //8 bits, one per block (0: Block type 0, 1: Block Type 1, from MSB to LSB)
    int nb_blocks {};     //How many blocks we've written so far; is reset every 8 blocks.
    InputIterator iter_current_byte {begin};
    SlidingWindowGBA sliding_window {};

    while (iter_current_byte != end)
    {
        /* Now, the general strategy is for each byte to check if the bunch of bytes following it
         * can already be found earlier in the same order in the sliding window (at least 3 bytes in a row). If it is,
         * we compress them as Block 1. Otherwise it's block 0. Doesn't get much simpler than this. */
        const auto pos_to_copy = find_position_to_copy(begin, iter_current_byte, end, sliding_window);
        if (pos_to_copy.nb_bytes_to_copy >= 3)   //Block 1
        {
            const char byte0 { static_cast<char>(((pos_to_copy.nb_bytes_to_copy - 3) << 4) |
                                                 static_cast<char>((pos_to_copy.distance_from_current >> 8) & 0xFF)) };
            out.push_back(byte0);
            out.push_back(pos_to_copy.distance_from_current & 0xFF);
            nb_blocks++;
            flag_byte = (flag_byte << 1) | 1;
            if (nb_blocks == 8)
            {
                nb_blocks = 0;
                out[position_latest_flag_byte] = flag_byte;
                flag_byte = 0;
                out.push_back(0) ; //Dummy flag byte for next eight blocks.
                position_latest_flag_byte = out.size() - 1;
            }
            for (int i = 0; i < pos_to_copy.nb_bytes_to_copy; i++)
            {
                sliding_window.Add_Byte(*iter_current_byte);
                iter_current_byte++;
            }
        }
        else   //Block type 0, the easiest. Just a single byte to copy from input to output.
        {
            out.push_back(*iter_current_byte);
            nb_blocks++;
            flag_byte <<= 1;
            if (nb_blocks == 8)
            {
                nb_blocks = 0;
                out[position_latest_flag_byte] = flag_byte;
                flag_byte = 0;
                out.push_back(0) ; //Dummy flag byte for next eight blocks.
                position_latest_flag_byte = out.size() - 1;
            }
            sliding_window.Add_Byte(*iter_current_byte);
            iter_current_byte++;
        }
    }
    //We're done. Write the last flag byte if at least one block was added since the previous.
    if (nb_blocks > 0)
    {
        flag_byte = (flag_byte) << (8 - nb_blocks);
        out[position_latest_flag_byte] = flag_byte;
    }
    else
    {
        out.pop_back();    //We just finished writing 8 blocks, therefore an extra dummy byte
                            //was added for the next flag byte that will never come. So get rid of it.
    }

    return out;
}

}
