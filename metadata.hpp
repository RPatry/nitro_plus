#ifndef METADATA_HPP
#define METADATA_HPP

#include <QtEndian>
#include <vector>
#include <memory>
#include "nitrofs.hpp"

struct NitroDirectoryMetadata;

struct NitroNodeMetadata
{
    explicit NitroNodeMetadata(const quint16 _id, NitroDirectoryMetadata* const _parent) : id{_id}, parent{_parent}
    {   }
    virtual ~NitroNodeMetadata() { }

    const quint16 id {};
    NitroDirectoryMetadata* const parent {};

    NitroDirectoryMetadata* Find_Parent_Node_with_Id(quint16 id_parent) const;
};

struct NitroFileMetadata;

struct NitroDirectoryMetadata : virtual NitroNodeMetadata
{
    explicit NitroDirectoryMetadata(const quint16 _id, NitroDirectoryMetadata* const _parent)
        : NitroNodeMetadata(_id, _parent)
    {   }

    std::vector<std::unique_ptr<NitroDirectoryMetadata>> children_directories {};
    std::vector<std::unique_ptr<NitroFileMetadata>> children_files {};
};

enum class CompressionType { Raw, LZ77 };

struct NitroFileMetadata : virtual NitroNodeMetadata
{
    explicit NitroFileMetadata(const quint16 id, NitroDirectoryMetadata* const parent) :
        NitroNodeMetadata(id, parent)
    {  }

    CompressionType compression_type { CompressionType::Raw };
};

struct NitroNARCMetadata : NitroDirectoryMetadata, NitroFileMetadata
{
    explicit NitroNARCMetadata(const quint16 id, NitroDirectoryMetadata * const parent)
        : NitroNodeMetadata(id, parent), NitroDirectoryMetadata(id, parent), NitroFileMetadata(id, parent) {  }
};

/*!
 * \brief Check the consistency between the configuration of a ROM and the filesystem that we parsed in the ROM.
 * \param The metadata about a directory.
 * \param A directory in the NitroROM filesystem
 */
bool Trees_are_Equal(NitroDirectoryMetadata const* const directory_metadata, NitroDirectory const* const directory);

/*!
 * \brief Try to apply a few heuristics to guess the compression type of a file, if any.
 */
CompressionType Guess_Compression_Type(MemoryStream const& nds_stream, NitroFile const* const file);

CompressionType Guess_Compression_Type(const MemoryStream &file_stream);

/*!
 * \brief Will parse the whole file system starting from a directory and try to guess the correct compression type for each file.
 * \return A tree structure, equivalent to the file system, storing metadata for each file.
 * \note Recursive call
 */
template<typename NitroDirMetadataType>
std::unique_ptr<NitroDirMetadataType> Parse_File_System_Guess_Metadata(MemoryStream &nds_or_narc_stream, NitroDirectory* const directory,
                                                                       NitroDirectoryMetadata * const parent = nullptr);

/*!
 * \brief Try to read the metadata for a ROM if metadata was already previously saved.
 * \param The game title, as found in the NDS header.
 * \param The path to the ROM itself.
 * \return A tree representing metadata for the file system. If no configuration was found, return an empty unique_ptr.
 */
std::unique_ptr<NitroDirectoryMetadata> Get_Metadata_for_ROM(QString const& rom_name_from_header, QString const& path_rom);

#endif // METADATA_HPP
