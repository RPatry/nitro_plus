#include "memorystream.hpp"
#include <QtEndian>

MemoryStream::MemoryStream(QByteArray &&bytes_stream) : bytes_stream{bytes_stream}
{

}

quint32 MemoryStream::Read_U32(const int pos) const
{
    quint32 result {};
    const quint8 byte3 { static_cast<quint8>(bytes_stream[pos + 3]) };
    const quint8 byte0 { static_cast<quint8>(bytes_stream[pos]) };
    const quint8 byte1 { static_cast<quint8>(bytes_stream[pos + 1]) };
    const quint8 byte2 { static_cast<quint8>(bytes_stream[pos + 2]) };
    result = byte0 | (byte1 << 8) | (byte2 << 16) | (byte3 << 24);
    return result;
}

quint16 MemoryStream::Read_U16(const int pos) const
{
    quint16 result {};
    const quint8 high_byte { static_cast<quint8>(bytes_stream[pos + 1]) };
    const quint8 low_byte { static_cast<quint8>(bytes_stream[pos]) };
    result = low_byte | (high_byte << 8);
    return result;
}

QString MemoryStream::Read_String(const int pos, const int str_size) const noexcept
{
    if (pos >= 0 && str_size >= 0 && pos < (Size() - str_size))
    {
        QByteArray array_for_string { bytes_stream.mid(pos, str_size) };
        return QString{array_for_string};
    }
    return "";
}
